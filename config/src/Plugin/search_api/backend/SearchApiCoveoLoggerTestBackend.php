<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo_logger_test\Plugin\search_api\backend;

use Drupal\Core\Logger\RfcLogLevel;
use Drupal\search_api_test\Plugin\search_api\backend\TestBackend;

/**
 * Provides a dummy backend for testing purposes.
 *
 * @SearchApiBackend(
 *   id = "search_api_coveo_logger_test",
 *   label = @Translation("Coveo Logger Test backend"),
 *   description = @Translation("Dummy backend implementation for logger test")
 * )
 */
class SearchApiCoveoLoggerTestBackend extends TestBackend {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'log_threshold' => RfcLogLevel::WARNING,
    ];
  }

}
