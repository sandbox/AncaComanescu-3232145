<?php

/**
 * @file
 * Hooks provided by the Search API Coveo search module.
 */

use Drupal\search_api\IndexInterface;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter Coveo objects before they are sent to Coveo for indexing.
 *
 * @param array $objects
 *
 *   An array of objects ready to be indexed, generated from $items array.
 * @param \Drupal\search_api\IndexInterface $index
 *   The search index for which items are being indexed.
 * @param \Drupal\search_api\Item\ItemInterface[] $items
 *   An array of items to be indexed, keyed by their item IDs.
 */
function hook_search_api_coveo_objects_alter(array &$objects, IndexInterface $index, array $items) {
  // Adds a "foo" field with value "bar" to all documents.
  foreach ($objects as $key => $object) {
    $objects[$key]['foo'] = 'bar';
  }
}

/**
 * Alter Coveo document ID which is a URI.
 *
 * @param string $document_id
 *   A Coveo document ID which is a URI.
 * @param \Drupal\search_api\IndexInterface $index
 *   A Search API index.
 * @param string $item_id
 *   A Search API item id.
 */
function hook_search_api_coveo_document_id_alter(&$document_id, IndexInterface &$index, &$item_id) {
  $document_id = 'https://somewhere.com' . parse_url($document_id, PHP_URL_PATH);
}

/**
 * @} End of "addtogroup hooks".
 */
