<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo_backend_test;

use Drupal\Core\State\StateInterface;
use Drupal\search_api_coveo\DataStructure\CoveoPushApiEndpoint;
use Drupal\search_api_coveo\SearchApiCoveoClientFactoryInterface;
use Drupal\search_api_coveo\SearchApiCoveoClientInterface;

/**
 * A service decoration that returns mock SearchApiCoveoClient.
 *
 * The mock client records the count of method calls to the state API. This
 * allows checking the client has been called by the backend when the backend in
 * called dynamically, such as in a indexing operation.
 */
final class CoveoTestClientFactory implements SearchApiCoveoClientFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(protected StateInterface $state) {}

  /**
   * {@inheritdoc}
   */
  public function getClient(string $sourceID, string $organizationID, string $apiKey, CoveoPushApiEndpoint $host, string $serverId): SearchApiCoveoClientInterface {
    return new MockCoveoTrackingClient($this->state);
  }

}
