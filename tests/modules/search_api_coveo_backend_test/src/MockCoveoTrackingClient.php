<?php

namespace Drupal\search_api_coveo_backend_test;

use Drupal\Core\State\StateInterface;
use Drupal\search_api_coveo\DataStructure\CoveoDocument;
use Drupal\search_api_coveo\DataStructure\CoveoFieldInfo;
use Drupal\search_api_coveo\DataStructure\CoveoFieldInfoSet;
use Drupal\search_api_coveo\DataStructure\CoveoFieldOrigin;
use Drupal\search_api_coveo\SearchApiCoveoClientInterface;

/**
 * A mock of SearchApiCoveoClient that tracks method calls.
 */
class MockCoveoTrackingClient implements SearchApiCoveoClientInterface {

  /**
   * Creates the mock client.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   Injected service.
   */
  public function __construct(protected StateInterface $state) {}

  /**
   * Internal method to track a method was called.
   *
   * @param string $methodName
   *   The method name to track.
   */
  protected function trackMethodCall(string $methodName) {
    $key = "mockCoveoTracking:$methodName";
    $count = $this->state->get($key, 0);
    ++$count;
    $this->state->set($key, $count);
  }

  /**
   * {@inheritdoc}
   */
  public function startBatch(): bool {
    $this->trackMethodCall('startBatch');
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function addDocument(CoveoDocument $document): bool {
    $this->trackMethodCall('addDocument');
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function endBatch(): bool {
    $this->trackMethodCall('endBatch');
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOlderThan(): bool {
    $this->trackMethodCall('deleteOlderThan');
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteItem(string $documentId): bool {
    $this->trackMethodCall('deleteItem');
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrganizations(): array {
    $this->trackMethodCall('getOrganizations');
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createFields(CoveoFieldInfoSet $bulk): bool {
    $this->trackMethodCall('createFields');
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function loadUserFields(): CoveoFieldInfoSet {
    $this->trackMethodCall('loadUserFields');
    return new CoveoFieldInfoSet([]);
  }

  /**
   * {@inheritdoc}
   */
  public function listFields(array $parameters, CoveoFieldOrigin $origin = CoveoFieldOrigin::User): CoveoFieldInfoSet {
    $this->trackMethodCall('listFields');
    return new CoveoFieldInfoSet([]);
  }

  /**
   * {@inheritdoc}
   */
  public function loadField(string $field_id): CoveoFieldInfo|false {
    $this->trackMethodCall('loadField');
    return new CoveoFieldInfo('id');
  }

  /**
   * {@inheritdoc}
   */
  public function loadSystemFields(): CoveoFieldInfoSet {
    $this->trackMethodCall('loadSystemFields');
    return new CoveoFieldInfoSet([]);
  }

  /**
   * {@inheritdoc}
   */
  public function updateFields(CoveoFieldInfoSet $bulk): array|false {
    $this->trackMethodCall('updateFields');
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteFields(CoveoFieldInfoSet $bulk): bool {
    $this->trackMethodCall('createFields');
    return TRUE;
  }

}
