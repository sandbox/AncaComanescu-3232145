<?php

declare(strict_types=1);

namespace Drupal\Tests\search_api_coveo\Kernel;

use Drupal\Core\State\StateInterface;
use Drupal\Tests\search_api\Kernel\BackendTestBase;

/**
 * Test the interaction of our backend plugin with Coveo.
 */
class SearchApiCoveoBackendTest extends BackendTestBase {

  /**
   * State is used to track SearchApiCoveoClient calls.
   */
  protected StateInterface $state;

  /**
   * The array of state keys used in MockCoveoTrackingClient.
   */
  protected array $clientStateKeys = [
    'mockCoveoTracking:startBatch',
    'mockCoveoTracking:addDocument',
    'mockCoveoTracking:endBatch',
    'mockCoveoTracking:deleteOlderThan',
    'mockCoveoTracking:deleteItem',
    'mockCoveoTracking:getOrganizations',
    'mockCoveoTracking:createFields',
    'mockCoveoTracking:loadUserFields',
    'mockCoveoTracking:listFields',
    'mockCoveoTracking:loadField',
    'mockCoveoTracking:loadSystemFields',
    'mockCoveoTracking:updateFields',
  ];

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'search_api_coveo',
    'search_api_coveo_backend_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $serverId = 'coveo_test_server';

  /**
   * {@inheritdoc}
   */
  protected $indexId = 'coveo_test_index';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $dummyCredentials = [
      'organization_id' => 'organization_id_value',
      'source_id' => 'source-id-value',
      'api_key' => 'api-key-value',
      'source_name' => 'Marketing_Test',
    ];
    $this->state = $this->container->get('state');
    $this->state->set('search_api_coveo.credentials.search_api_coveo', $dummyCredentials);
    $this->installConfig(['search_api_coveo_backend_test']);
  }

  /**
   * {@inheritdoc}
   */
  protected function updateIndex() {
    // Clear any tracking.
    $this->state->deleteMultiple($this->clientStateKeys);
    $index = $this->getIndex();
    // Remove a field from the index and check if the change is matched in the
    // server configuration.
    $field = $index->getField('keywords');
    if (!$field) {
      throw new \Exception();
    }
    $index->removeField('keywords');
    $index->save();

    $this->assertEquals(1, $this->state->get('mockCoveoTracking:createFields'));
    $this->assertEquals(1, $this->state->get('mockCoveoTracking:loadUserFields'));
  }

  /**
   * {@inheritdoc}
   */
  protected function clearIndex() {
    // Clear any tracking.
    $this->state->deleteMultiple($this->clientStateKeys);
    parent::clearIndex();
    $this->assertEquals(1, $this->state->get('mockCoveoTracking:deleteOlderThan'));
  }

  /**
   * {@inheritdoc}
   */
  protected function indexItems($index_id) {
    // Clear any tracking.
    $this->state->deleteMultiple($this->clientStateKeys);
    $returnValue = parent::indexItems($index_id);

    $this->assertEquals(1, $this->state->get('mockCoveoTracking:startBatch'));
    $this->assertEquals(1, $this->state->get('mockCoveoTracking:endBatch'));
    $this->assertEquals(5, $this->state->get('mockCoveoTracking:addDocument'));

    return $returnValue;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkBackendSpecificFeatures() {
    /* Check system fields */

    // Clear any tracking.
    $this->state->deleteMultiple($this->clientStateKeys);
    /** @var \Drupal\search_api_coveo\Plugin\search_api\backend\SearchApiCoveoBackend $backend */
    $backend = $this->getServer()->getBackend();
    $backend->getCoveoSystemFields();

    $this->assertEquals(1, $this->state->get('mockCoveoTracking:loadSystemFields'));

    /* Check delete items */

    // Clear any tracking.
    $this->state->deleteMultiple($this->clientStateKeys);
    $backend = $this->getServer()->getBackend();
    $index = $this->getIndex();
    $backend->deleteItems($index, ['node/1']);
    $this->assertEquals(1, $this->state->get('mockCoveoTracking:deleteItem'));
  }

  /* Skip the upstream regression tests. */

  /**
   * {@inheritdoc}
   */
  protected function regressionTests() {}

  /**
   * {@inheritdoc}
   */
  protected function regressionTests2() {}

  /* We are not currently supporting direct searches. */

  /**
   * {@inheritdoc}
   */
  protected function searchSuccess() {}

  /**
   * {@inheritdoc}
   */
  protected function searchNoResults() {}

  /**
   * {@inheritdoc}
   */
  protected function checkIndexWithoutFields() {
    return $this->getIndex();
  }

}
