<?php

namespace Drupal\Tests\search_api_coveo\Kernel;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\KernelTests\KernelTestBase;
use Drupal\search_api_coveo\Plugin\search_api\backend\SearchApiCoveoBackend;
use Drupal\search_api_coveo\SearchApiCoveoClientFactory;
use Drupal\search_api_coveo\SearchApiCoveoLoggerFactory;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Response;

/**
 * A testing base class.
 */
abstract class SearchApiCoveoKernelTestBase extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'search_api_coveo',
    'search_api',
  ];

  /**
   * The entity type manager service is normally injected.
   */
  protected EntityTypeManagerInterface $entityTypeManager;


  /**
   * The logger factory is normally injected.
   */
  protected SearchApiCoveoLoggerFactory $loggerFactory;

  /**
   * A handler for testing requests.
   */
  protected MockHandler $handler;

  /**
   * A stack for testing with history.
   */
  protected HandlerStack $stack;

  /**
   * Array to hold response history.
   *
   * @var array
   */
  protected array $history = [];

  /**
   * An http client factory for testing.
   */
  protected ClientFactory $httpClientFactory;

  /**
   * A search_api coveo client factory for testing.
   */
  protected SearchApiCoveoClientFactory $coveoClientFactory;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('search_api_index');
    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->loggerFactory = new SearchApiCoveoLoggerFactory($this->entityTypeManager, $this->container);
    // Use $this->handler->append() to add responses to the queue.
    $this->handler = new MockHandler();
    $this->stack = HandlerStack::create($this->handler);
    // Add a history tracker, which is used as a first in, first out queue:.
    // $roundTrip = array_shift($this->history);
    // $roundTrip will now be an array with a request
    // and response key/value pair.
    $history = Middleware::history($this->history);
    $this->stack->push($history);
    // Now build the factory with our handler stack.
    $this->httpClientFactory = new ClientFactory($this->stack);
    $mockMessenger = $this->createMock('Drupal\Core\Messenger\MessengerInterface');
    $this->coveoClientFactory = new SearchApiCoveoClientFactory($this->loggerFactory, $this->httpClientFactory, $mockMessenger);
  }

  /**
   * Helper function to create a server for the logger test.
   *
   * @param int $logLevel
   *   A configurable log level.
   *
   * @return string
   *   The server id.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function serverCreate(int $logLevel = RfcLogLevel::WARNING): string {
    $server_data = [
      'id' => 'test_server',
      'name' => 'Test server',
      'backend' => 'search_api_coveo',
    ];
    $server = $this->entityTypeManager->getStorage('search_api_server')->create($server_data);
    $this->assertInstanceOf(SearchApiCoveoBackend::class, $server->getBackend(), 'The newly created server is not using our Coveo backend.');
    // Mock the logging setting.
    $server->setBackendConfig([
      'log_threshold' => $logLevel,
    ]);
    $server->save();
    return $server->id();
  }

  /**
   * Helper function for building responses..
   *
   * @param int $code
   *   The HTTP code to return.
   * @param array $json
   *   An array to encode as json.
   *
   * @return \GuzzleHttp\Psr7\Response
   *   A prepared Coveo response.
   */
  protected function buildResponse(int $code, array $json = []): Response {
    $body = NULL;
    // Encode json if provided..
    if (!empty($json)) {
      $body = json_encode($json);
      $body = $body === FALSE ? NULL : $body;
    }
    $response = new Response(
      $code,
      ['Content-Type' => 'application/json'],
      $body
    );
    return $response;
  }

}
