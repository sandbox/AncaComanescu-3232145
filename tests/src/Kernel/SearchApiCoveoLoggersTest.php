<?php

declare(strict_types=1);

namespace Drupal\Tests\search_api_coveo\Kernel;

use Drupal\Core\Logger\RfcLogLevel;

/**
 * Test our custom logger.
 *
 * @group search_api_coveo
 */
final class SearchApiCoveoLoggersTest extends SearchApiCoveoKernelTestBase {

  /**
   * Test the factory method.
   *
   * @covers \Drupal\search_api_coveo\SearchApiCoveoLoggerFactory::get
   * @covers \Drupal\search_api_coveo\SearchApiCoveoLoggerChannel::log
   */
  public function testGet(): void {
    $server = $this->serverCreate(RfcLogLevel::NOTICE);
    $channel = $this->loggerFactory->get($server);
    $mockLogger = $this->createMock('\Drupal\dblog\Logger\DbLog');
    $mockLogger->expects($this->never())->method('log');
    $channel->setLoggers([$mockLogger]);
    $channel->log(RfcLogLevel::INFO, 'Test log');

  }

}
