<?php

namespace Drupal\Tests\search_api_coveo\Kernel;

use Drupal\search_api_coveo\DataStructure\CoveoDocument;
use Drupal\search_api_coveo\DataStructure\CoveoFieldInfo;
use Drupal\search_api_coveo\DataStructure\CoveoFieldInfoSet;
use Drupal\search_api_coveo\DataStructure\CoveoPushApiEndpoint;
use Drupal\search_api_coveo\SearchApiCoveoClientInterface;

/**
 * Verify the SearchApiCoveoClient.
 *
 * @see \Drupal\Tests\search_api_coveo\Kernel\SearchApiCoveoKernelTestBase::setUp()
 * @see \Drupal\Tests\search_api_coveo\Kernel\SearchApiCoveoKernelTestBase::buildResponse()
 *
 * @coversDefaultClass \Drupal\search_api_coveo\SearchApiCoveoClient
 */
class SearchApiCoveoClientTest extends SearchApiCoveoKernelTestBase {

  /**
   * A client with a mocked Guzzle handler.
   */
  protected SearchApiCoveoClientInterface $coveoClient;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->coveoClient = $this->coveoClientFactory->getClient(
      sourceID: 'source-id',
      organizationID: 'org-id',
      apiKey: 'api-key',
      host: CoveoPushApiEndpoint::Dev,
      serverId: $this->serverCreate(),
    );
  }

  /**
   * Test start batch and internal helpers.
   *
   * @covers ::startBatch
   * @covers ::setStartTime
   * @covers ::updateCoveoStatus
   * @covers ::post
   */
  public function testStartBatch(): void {
    $this->handler->append($this->buildResponse(202));
    $this->coveoClient->startBatch();
    $roundTrip = array_shift($this->history);
    $request = $roundTrip['request'] ?? NULL;
    /* ===================================================================== */
    // Check the request.
    $this->assertInstanceOf('\Psr\Http\Message\RequestInterface', $request);
    // Check that the request is the proper method.
    $this->assertEquals('POST', $request->getMethod());
    // Check the query.
    parse_str($request->getUri()->getQuery(), $queryParameters);
    $this->assertTrue(array_key_exists('statusType', $queryParameters));
    $this->assertEquals('REBUILD', $queryParameters['statusType']);
    // Check the path.
    $expectedPath = '/push/v1/organizations/org-id/sources/source-id/status';
    $this->assertEquals($expectedPath, $request->getUri()->getPath());
    // Check the headers.
    $headers = $request->getHeaders();
    $authorization = array_shift($headers['Authorization']);
    $this->assertEquals('Bearer api-key', $authorization);
  }

  /**
   * Test add document method and internal helpers.
   *
   * @covers ::addDocument
   * @covers ::addCoveoDocument
   */
  public function testAddSingleDocument(): void {
    $date = new \DateTime();
    $document = new CoveoDocument('https://example.com/document', $date, $date, $this->loggerFactory->get('test_server'));
    // Add a single document.
    $this->assertTrue($this->coveoClient->addDocument($document));
  }

  /**
   * Test add document method and internal helpers.
   *
   * @covers ::endBatch
   * @covers ::pushCoveoBatch
   * @covers ::getCoveoS3FileData
   * @covers ::put
   */
  public function testEndBatch(): void {
    $startResponse = $this->buildResponse(202);
    $s3Response = $this->buildResponse(201, [
      'fileId' => 'file-id',
      'requiredHeaders' => [
        'property1' => 'string-1',
        'property2' => 'string-2',
      ],
      'uploadUri' => 'https://example.com/upload-path-returned',
    ]);
    $uploadResponse = $this->buildResponse(201);
    $notifyResponse = $this->buildResponse(202);
    $endResponse = $this->buildResponse(202);
    $this->handler->append($startResponse, $s3Response, $uploadResponse, $notifyResponse, $endResponse);
    $this->coveoClient->startBatch();
    $date = new \DateTime();
    $document = new CoveoDocument('https://example.com/document', $date, $date, $this->loggerFactory->get('test_server'));
    // Add a single document.
    $this->assertTrue($this->coveoClient->addDocument($document));
    $this->coveoClient->endBatch();
    /* ===================================================================== */
    // The 5 responses loaded above should have been returned.
    $this->assertEquals(5, count($this->history));
    // Start is tested in a prior method.
    array_shift($this->history);
    // Check the S3 Response.
    $roundTrip = array_shift($this->history);
    $request = $roundTrip['request'] ?? NULL;
    $this->assertInstanceOf('\Psr\Http\Message\RequestInterface', $request);
    // Check that the request is the proper method.
    $this->assertEquals('POST', $request->getMethod());
    // Check the path.
    $expectedPath = '/push/v1/organizations/org-id/files';
    $this->assertEquals($expectedPath, $request->getUri()->getPath());
    // Check the data push.
    $roundTrip = array_shift($this->history);
    $request = $roundTrip['request'] ?? NULL;
    $this->assertInstanceOf('\Psr\Http\Message\RequestInterface', $request);
    // Check that the request is the proper method.
    $this->assertEquals('PUT', $request->getMethod());
    // Check the path.
    $expectedPath = '/upload-path-returned';
    // Check the body content.
    $this->assertEquals($expectedPath, $request->getUri()->getPath());
    $body = json_decode((string) $request->getBody(), TRUE);
    $this->assertTrue(array_key_exists('addOrUpdate', $body));
    $this->assertNotEmpty($body['addOrUpdate']);
    // Check the notify push.
    $roundTrip = array_shift($this->history);
    $request = $roundTrip['request'] ?? NULL;
    $this->assertInstanceOf('\Psr\Http\Message\RequestInterface', $request);
    // Check that the request is the proper method.
    $this->assertEquals('PUT', $request->getMethod());
    // Check that the request is the proper method.
    $expectedPath = '/push/v1/organizations/org-id/sources/source-id/documents/batch';
    $this->assertEquals($expectedPath, $request->getUri()->getPath());
    // Check the query parameters.
    parse_str($request->getUri()->getQuery(), $queryParameters);
    $this->assertTrue(array_key_exists('fileId', $queryParameters));
    $this->assertTrue(array_key_exists('orderingId', $queryParameters));
    $this->assertEquals('file-id', $queryParameters['fileId']);
    $this->assertIsNumeric($queryParameters['orderingId']);
    $this->assertTrue((int) $queryParameters['orderingId'] > 0);
    // Check the return to idle status update.
    $roundTrip = array_shift($this->history);
    $request = $roundTrip['request'] ?? NULL;
    $this->assertInstanceOf('\Psr\Http\Message\RequestInterface', $request);
    // Check that the request is the proper method.
    $this->assertEquals('POST', $request->getMethod());
    // Check the query.
    parse_str($request->getUri()->getQuery(), $queryParameters);
    $this->assertTrue(array_key_exists('statusType', $queryParameters));
    $this->assertEquals('IDLE', $queryParameters['statusType']);
    // Check the path.
    $expectedPath = '/push/v1/organizations/org-id/sources/source-id/status';
    $this->assertEquals($expectedPath, $request->getUri()->getPath());
  }

  /**
   * Test deleteOlderThan method and internal helpers.
   *
   * @covers ::deleteOlderThan
   * @covers ::deleteCoveoByTime
   * @covers ::delete
   * @covers ::setStartTime
   */
  public function testDeleteOlder(): void {
    $this->handler->append($this->buildResponse(202));
    $this->coveoClient->deleteOlderThan();
    /* ===================================================================== */
    // Check the request.
    $roundTrip = array_shift($this->history);
    $request = $roundTrip['request'] ?? NULL;
    $this->assertInstanceOf('\Psr\Http\Message\RequestInterface', $request);
    // Check that the request is the proper method.
    $this->assertEquals('DELETE', $request->getMethod());
    // Check the path.
    $expectedPath = '/push/v1/organizations/org-id/sources/source-id/documents/olderthan';
    $this->assertEquals($expectedPath, $request->getUri()->getPath());
    // Check the query parameters.
    parse_str($request->getUri()->getQuery(), $queryParameters);
    $this->assertTrue(array_key_exists('orderingId', $queryParameters));
    $this->assertIsNumeric($queryParameters['orderingId']);
    $this->assertTrue((int) $queryParameters['orderingId'] > 0);
  }

  /**
   * Test add deleteOlderThan method and internal helpers.
   *
   * @covers ::deleteItem
   * @covers ::deleteCoveoDocument
   * @covers ::delete
   */
  public function testDeleteItem(): void {
    // This call generates three requests.
    $this->handler->append($this->buildResponse(202));
    $this->handler->append($this->buildResponse(202));
    $this->handler->append($this->buildResponse(202));
    $this->coveoClient->deleteItem('item-id');
    /* ===================================================================== */
    // Check for status push.
    $roundTrip = array_shift($this->history);
    $request = $roundTrip['request'] ?? NULL;
    $this->assertInstanceOf('\Psr\Http\Message\RequestInterface', $request);
    $this->assertEquals('POST', $request->getMethod());
    parse_str($request->getUri()->getQuery(), $queryParameters);
    $this->assertTrue(array_key_exists('statusType', $queryParameters));
    $this->assertEquals('REBUILD', $queryParameters['statusType']);
    $expectedPath = '/push/v1/organizations/org-id/sources/source-id/status';
    $this->assertEquals($expectedPath, $request->getUri()->getPath());
    // Check for deletion.
    $roundTrip = array_shift($this->history);
    $request = $roundTrip['request'] ?? NULL;
    $this->assertInstanceOf('\Psr\Http\Message\RequestInterface', $request);
    $this->assertEquals('DELETE', $request->getMethod());
    $expectedPath = '/push/v1/organizations/org-id/sources/source-id/documents';
    $this->assertEquals($expectedPath, $request->getUri()->getPath());
    parse_str($request->getUri()->getQuery(), $queryParameters);
    $this->assertTrue(array_key_exists('documentId', $queryParameters));
    $this->assertEquals('item-id', $queryParameters['documentId']);
    // Check for status push.
    $roundTrip = array_shift($this->history);
    $request = $roundTrip['request'] ?? NULL;
    $this->assertInstanceOf('\Psr\Http\Message\RequestInterface', $request);
    $this->assertEquals('POST', $request->getMethod());
    // Check the query.
    parse_str($request->getUri()->getQuery(), $queryParameters);
    $this->assertTrue(array_key_exists('statusType', $queryParameters));
    $this->assertEquals('IDLE', $queryParameters['statusType']);
    // Check the path.
    $expectedPath = '/push/v1/organizations/org-id/sources/source-id/status';
    $this->assertEquals($expectedPath, $request->getUri()->getPath());
  }

  /**
   * Test getOrganizations method and internal helpers.
   *
   * @covers ::getOrganizations
   * @covers ::get
   */
  public function testOrganizations(): void {
    $this->handler->append($this->buildResponse(201));
    $this->coveoClient->getOrganizations();
    /* ===================================================================== */
    // Check the request.
    $roundTrip = array_shift($this->history);
    $request = $roundTrip['request'] ?? NULL;
    $this->assertInstanceOf('\Psr\Http\Message\RequestInterface', $request);
    // Check that the request is the proper method.
    $this->assertEquals('GET', $request->getMethod());
    // Check the path.
    $expectedPath = '/push/v1/rest/organizations/';
    $this->assertEquals($expectedPath, $request->getUri()->getPath());
  }

  /**
   * Test createFields method and internal helpers.
   *
   * @covers ::createFields
   * @covers ::post
   */
  public function testCreateFields(): void {
    // Test for empty.
    $empty = new CoveoFieldInfoSet(NULL);
    $this->assertFalse($this->coveoClient->createFields($empty));
    $this->handler->append($this->buildResponse(204));
    // Test with our standard set.
    $testSet = $this->getFieldInfoSet();
    $this->coveoClient->createFields($testSet);
    /* ===================================================================== */
    // Check the request.
    $roundTrip = array_shift($this->history);
    $request = $roundTrip['request'] ?? NULL;
    $this->assertInstanceOf('\Psr\Http\Message\RequestInterface', $request);
    // Check that the request is the proper method.
    $this->assertEquals('POST', $request->getMethod());
    // Check the path.
    $expectedPath = '/rest/organizations/org-id/indexes/fields/batch/create';
    $this->assertEquals($expectedPath, $request->getUri()->getPath());
    // Check the body content.
    $expectedBody = json_encode($testSet);
    $this->assertEquals($expectedBody, $request->getBody());
  }

  /**
   * Test listFields method and internal helpers.
   *
   * @covers ::listFields
   * @covers ::loadUserFields
   */
  public function testListFields(): void {
    $fieldsResponse = $this->buildResponse(201, [
      'items' => $this->getFieldInfoSet()->toArray(),
      'totalEntries' => 6,
      'totalPages' => 1,
    ]);
    $this->handler->append($fieldsResponse);
    $this->coveoClient->loadUserFields();
    /* ===================================================================== */
    // Check the request.
    $roundTrip = array_shift($this->history);
    $request = $roundTrip['request'] ?? NULL;
    $this->assertInstanceOf('\Psr\Http\Message\RequestInterface', $request);
    // Check that the request is the proper method.
    $this->assertEquals('POST', $request->getMethod());
    // Check the path.
    $expectedPath = '/rest/organizations/org-id/indexes/fields/search';
    $this->assertEquals($expectedPath, $request->getUri()->getPath());
    // Check the body content.
    $expectedBody = json_encode([
      'filters' => [
        'facet' => 'ALL',
        'sort' => 'ALL',
        'origin' => 'USER',
      ],
      'page' => 0,
    ]);
    $this->assertEquals($expectedBody, $request->getBody());
  }

  /**
   * Test listFields method and internal helpers.
   *
   * @covers ::loadField
   * @covers ::get
   */
  public function testLoadField(): void {
    $fieldInfo = $this->getFieldInfoSet()->toArray();
    $singleFieldData = array_shift($fieldInfo);
    $fieldsResponse = $this->buildResponse(201, $singleFieldData->toArray());
    $this->handler->append($fieldsResponse);
    $field = $this->coveoClient->loadField('test_item_0');
    /* ===================================================================== */
    // Check the return value.
    $this->assertInstanceOf('\Drupal\search_api_coveo\DataStructure\CoveoFieldInfo', $field);
    $this->assertEquals('test_item_0', $field->name);
    // Check the request.
    $roundTrip = array_shift($this->history);
    $request = $roundTrip['request'] ?? NULL;
    $this->assertInstanceOf('\Psr\Http\Message\RequestInterface', $request);
    // Check that the request is the proper method.
    $this->assertEquals('GET', $request->getMethod());
    // Check the path.
    $expectedPath = '/rest/organizations/org-id/indexes/fields/test_item_0';
    $this->assertEquals($expectedPath, $request->getUri()->getPath());
  }

  /**
   * Test createFields method and internal helpers.
   *
   * @covers ::updateFields
   * @covers ::put
   */
  public function testUpdateFields(): void {
    // Test for empty.
    $empty = new CoveoFieldInfoSet(NULL);
    $this->assertFalse($this->coveoClient->updateFields($empty));
    $this->handler->append($this->buildResponse(204));
    // Test with our standard set.
    $testSet = $this->getFieldInfoSet();
    $this->coveoClient->updateFields($testSet);
    /* ===================================================================== */
    // Check the request.
    $roundTrip = array_shift($this->history);
    $request = $roundTrip['request'] ?? NULL;
    $this->assertInstanceOf('\Psr\Http\Message\RequestInterface', $request);
    // Check that the request is the proper method.
    $this->assertEquals('PUT', $request->getMethod());
    // Check the path.
    $expectedPath = '/rest/organizations/org-id/indexes/fields/batch/update';
    $this->assertEquals($expectedPath, $request->getUri()->getPath());
    // Check the body content.
    $expectedBody = json_encode($testSet);
    $this->assertEquals($expectedBody, $request->getBody());
  }

  /**
   * Test createFields method and internal helpers.
   *
   * @covers ::updateFields
   * @covers ::put
   */
  public function testDeleteFields(): void {
    // Test for empty.
    $empty = new CoveoFieldInfoSet(NULL);
    $this->assertFalse($this->coveoClient->updateFields($empty));
    $this->handler->append($this->buildResponse(204));
    // Test with our standard set.
    $testSet = $this->getFieldInfoSet();
    $this->coveoClient->deleteFields($testSet);
    /* ===================================================================== */
    // Check the request.
    $roundTrip = array_shift($this->history);
    $request = $roundTrip['request'] ?? NULL;
    $this->assertInstanceOf('\Psr\Http\Message\RequestInterface', $request);
    // Check that the request is the proper method.
    $this->assertEquals('DELETE', $request->getMethod());
    // Check the path.
    $expectedPath = '/rest/organizations/org-id/indexes/fields/batch/delete';
    // Check the query parameters.
    $this->assertEquals($expectedPath, $request->getUri()->getPath());
    parse_str($request->getUri()->getQuery(), $queryParameters);
    $names = explode(',', array_shift($queryParameters));
    $this->assertEquals(6, count($names));
  }

  /**
   * Helper function to construct a set for testing.
   *
   * @return \Drupal\search_api_coveo\DataStructure\CoveoFieldInfoSet
   *   A populated set.
   */
  protected function getFieldInfoSet(): CoveoFieldInfoSet {
    $types = ['LONG', 'LONG_64', 'DOUBLE', 'DATE', 'VECTOR', 'STRING'];
    $testInfo = [];
    foreach ($types as $index => $type) {
      $testInfo[] = new CoveoFieldInfo(
        name: 'test_item_' . $index,
        type: $type
      );
    }
    return new CoveoFieldInfoSet($testInfo);
  }

}
