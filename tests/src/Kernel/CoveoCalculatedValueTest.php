<?php

declare(strict_types=1);

namespace Drupal\Tests\search_api_coveo\Kernel;

use Drupal\Core\Security\Attribute\TrustedCallback;
use Drupal\node\Entity\Node;
use Drupal\search_api\Item\Field;
use Drupal\search_api\Utility\Utility;
use Drupal\Tests\search_api\Kernel\Processor\ProcessorTestBase;

/**
 * Tests CoveoCalculatedField processor plugin.
 *
 * @coversDefaultClass \Drupal\search_api_coveo\Plugin\search_api\processor\CoveoCalculatedValue
 * @group search_api_coveo
 */
final class CoveoCalculatedValueTest extends ProcessorTestBase {

  /**
   * The nodes created for testing.
   *
   * @var \Drupal\node\Entity\Node[]
   */
  protected $entities = [];

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['search_api_coveo'];

  /**
   * {@inheritdoc}
   */
  public function setUp($processor = NULL): void {
    parent::setUp('coveo_calculated_value');

    $field = new Field($this->index, 'coveo_calculated_value_node');
    $field->setType('string');
    $field->setPropertyPath('coveo_calculated_value');
    $field->setLabel('Item Type');
    $field->setConfiguration(['callable' => '\Drupal\Tests\search_api_coveo\Kernel\CoveoCalculatedValueCallbacks::testValueCallback']);
    $this->index->addField($field);
    $this->index->save();

    // Create a test node and test comment.
    $this->entities['test_1'] = Node::create([
      'title' => 'Test 1',
      'type' => 'article',
    ]);
    $this->entities['test_1']->save();
    // Create a test node and test comment.
    $this->entities['test_2'] = Node::create([
      'title' => 'Test 2',
      'type' => 'article',
    ]);
    $this->entities['test_2']->save();
  }

  /**
   * Tests the field calculated fields for search items.
   *
   * @covers ::addFieldValues
   */
  public function testItemFieldExtraction() {
    foreach ($this->entities as $node) {
      $id = Utility::createCombinedId('entity:node', $node->id() . ':en');
      $item = \Drupal::getContainer()
        ->get('search_api.fields_helper')
        ->createItemFromObject($this->index, $node->getTypedData(), $id);
      // Extract field values and check the value of our field.
      $fields = $item->getFields();
      $expected = [$node->label() . '-processed-by-callback'];
      $this->assertEquals($expected, $fields['coveo_calculated_value_node']->getValues());
    }
  }

}

/**
 * Callbacks for the CoveoCalculatedValueTest.
 */
final class CoveoCalculatedValueCallbacks {

  /**
   * Callback for the testValueCallback.
   *
   * @param \Drupal\node\Entity\Node $object
   *   The node object.
   *
   * @return string
   *   The processed value.
   */
  #[TrustedCallback]
  public static function testValueCallback(Node $object): string {
    return $object->label() . '-processed-by-callback';
  }

}
