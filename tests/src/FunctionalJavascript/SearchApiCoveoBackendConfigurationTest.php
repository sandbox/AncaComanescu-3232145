<?php

declare(strict_types=1);

namespace Drupal\Tests\search_api_coveo\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests configuration form for our backend plugin.
 *
 * @group search_api_coveo
 */
final class SearchApiCoveoBackendConfigurationTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['search_api', 'search_api_coveo'];

  /**
   * Tests that adding a server works.
   */
  public function testAddingServer() {
    $admin_user = $this->drupalCreateUser(['administer search_api', 'access content']);
    $this->drupalLogin($admin_user);

    $this->drupalGet('admin/config/search/search-api/add-server');
    $page = $this->getSession()->getPage();
    /** @var \Drupal\FunctionalJavascriptTests\WebDriverWebAssert $assert_session*/
    $assert_session = $this->assertSession();
    $assert_session->pageTextContains('Add search server');

    $page->fillField('name', ' ~`Test Server');
    $machine_name = $assert_session->waitForElementVisible('css', '[name="name"] + * .machine-name-value');
    $this->assertNotEmpty($machine_name);
    $page->findButton('Edit')->press();
    $page->fillField('id', '_test');
    $page->fillField('backend_config[search_api_coveo][organization_id]', 'org-id');
    $page->fillField('backend_config[search_api_coveo][source_id]', 'source-id');
    $page->fillField('backend_config[search_api_coveo][source_name]', 'source-name');
    $page->fillField('backend_config[search_api_coveo][api_key]', 'api-key');
    $page->pressButton('Save');

    $assert_session->addressEquals('admin/config/search/search-api/server/_test');
    $this->drupalGet('admin/config/search/search-api/server/_test');
    $assert_session = $this->assertSession();
    $assert_session->pageTextContains('org-id');
    $assert_session->pageTextContains('source-name');
  }

}
