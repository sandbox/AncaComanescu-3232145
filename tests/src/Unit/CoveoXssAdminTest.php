<?php

declare(strict_types=1);

namespace Drupal\Tests\search_api_coveo\Unit;

use Drupal\search_api_coveo\Plugin\search_api\processor\CoveoXssAdmin;
use Drupal\Tests\search_api\Unit\Processor\ProcessorTestTrait;
use Drupal\Tests\search_api\Unit\Processor\TestItemsTrait;
use Drupal\Tests\UnitTestCase;

/**
 * Test description.
 *
 * @group search_api_coveo
 */
final class CoveoXssAdminTest extends UnitTestCase {

  use ProcessorTestTrait;
  use TestItemsTrait;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->setUpMockContainer();

    $this->processor = new CoveoXssAdmin([], 'xss_admin_filter', []);
  }

  /**
   * Tests preprocessing field values with script and style tags.
   *
   * @param string $passed_value
   *   The value that should be passed into process().
   * @param string $expected_value
   *   The expected processed value.
   *
   * @dataProvider providerXssFilter
   */
  public function testXssFilter(string $passed_value, string $expected_value): void {
    $this->invokeMethod('processFieldValue', [&$passed_value, 'text']);
    $this->assertEquals($expected_value, $passed_value);
  }

  /**
   * Data provider for testXssFilter().
   *
   * @return array
   *   An array of test cases.
   */
  public static function providerXssFilter(): array {
    return [
      ['<script type="text/javascript" src="/files/js/foo.js" ></script>script-tag', 'script-tag'],
      ['<style>p {color: #26b72b;}</style>style-tag', 'p {color: #26b72b;}style-tag'],
    ];
  }

}
