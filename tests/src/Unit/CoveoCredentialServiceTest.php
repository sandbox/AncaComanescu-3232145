<?php

declare(strict_types=1);

namespace Drupal\Tests\search_api_coveo\Unit;

use Drupal\Core\State\State;
use Drupal\search_api_coveo\CredentialProvider;
use Drupal\search_api_coveo_keys\RepositoryManager;
use Drupal\Tests\UnitTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Test the credential service..
 *
 * @group search_api_coveo
 */
final class CoveoCredentialServiceTest extends UnitTestCase {

  /**
   * Mock the state service.
   */
  protected State|MockObject $mockState;

  /**
   * Mock the repository manager.
   */
  protected RepositoryManager|MockObject $mockKeyRepository;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->mockState = $this->createMock('Drupal\Core\State\State');
    $this->mockState->expects($this->any())
      ->method('get')
      ->with('cred-key')
      ->willReturn([
        'organization_id' => 'org-id',
        'source_id' => 'source-id',
        'api_key' => 'api-key',
        'source_name' => 'source-name',
      ]);
    $this->mockKeyRepository = $this->createMock('\Drupal\search_api_coveo_keys\RepositoryManager');
    $this->mockKeyRepository->expects($this->any())
      ->method('getKeyValues')
      ->with('cred-key')
      ->willReturn([
        'organization_id' => 'key-org-id',
        'source_id' => 'key-source-id',
        'api_key' => 'key-api-key',
        'source_name' => 'key-source-name',
      ]);
  }

  /**
   * Tests something.
   */
  public function testCredentialService(): void {
    $credentialService = new CredentialProvider($this->mockState);
    $credentials = $credentialService->getCredentials([
      'credential_provider' => 'search_api_coveo',
      'storage_key' => 'cred-key',
    ]);
    $expected = [
      'organization_id' => 'org-id',
      'source_id' => 'source-id',
      'api_key' => 'api-key',
      'source_name' => 'source-name',
    ];
    $this->assertEquals($expected, $credentials);
    // Now re-test with Key.
    $credentialService->setKeyRepository($this->mockKeyRepository);
    $this->assertTrue($credentialService->additionalProviders());
    $credentials = $credentialService->getCredentials([
      'credential_provider' => 'key',
      'storage_key' => 'cred-key',
    ]);
    $expected = [
      'organization_id' => 'key-org-id',
      'source_id' => 'key-source-id',
      'api_key' => 'key-api-key',
      'source_name' => 'key-source-name',
    ];
    $this->assertEquals($expected, $credentials);
  }

}
