<?php

declare(strict_types=1);

namespace Drupal\Tests\search_api_coveo\Unit;

use Drupal\search_api_coveo\DataStructure\CoveoFieldInfo;
use Drupal\search_api_coveo\DataStructure\CoveoFieldInfoSet;
use Drupal\Tests\UnitTestCase;

/**
 * Tests those methods unique to CoveoFieldInfoSet.
 *
 * @group data_structures
 *
 * @coversDefaultClass \Drupal\search_api_coveo\DataStructure\CoveoFieldInfoSet
 */
final class CoveoFieldInfoSetTest extends UnitTestCase {

  /**
   * Preconfigured field info instance for set population.
   *
   * @var \Drupal\search_api_coveo\DataStructure\CoveoFieldInfo[]
   */
  protected array $fieldInfoItems;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->fieldInfoItems = [];
    $types = ['LONG', 'LONG_64', 'DOUBLE', 'DATE', 'VECTOR', 'STRING'];
    foreach ($types as $index => $type) {
      $this->fieldInfoItems[] = new CoveoFieldInfo(
        name: 'test_item_' . $index,
        type: $type
      );
    }
  }

  /**
   * Verify set creation.
   *
   * @covers ::add
   * @covers ::has
   */
  public function testConstructor(): void {
    $set = new CoveoFieldInfoSet($this->fieldInfoItems);
    foreach ($this->fieldInfoItems as $value) {
      $this->assertTrue($set->has($value));
    }
    $this->assertFalse($set->has('foo'));
    // Test type safety.
    $this->expectException(\TypeError::class);
    $set = new CoveoFieldInfoSet([2]);
  }

}
