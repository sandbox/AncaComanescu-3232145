<?php

declare(strict_types=1);

namespace Drupal\Tests\search_api_coveo\Unit;

use Drupal\search_api_coveo\DataStructure\CoveoFieldInfo;
use Drupal\Tests\UnitTestCase;

/**
 * Test description.
 *
 * @group search_api_coveo
 */
final class CoveoFieldInfoTest extends UnitTestCase {

  /**
   * Tests the validation in the constructor.
   */
  public function testInvalidName(): void {
    // This is not a valid name.
    $this->expectException(\ValueError::class);
    $info = new CoveoFieldInfo(
      name: 'stringTest',
      type: 'STRING'
    );
  }

  /**
   * Tests the validation in the constructor.
   *
   * @covers \Drupal\search_api_coveo\DataStructure\CoveoFieldDataType
   *
   * @dataProvider providerTypes
   */
  public function testValidNameType($type): void {
    // This is a valid name and valid type.
    $info = new CoveoFieldInfo(
      name: 'string_test',
      type: $type
    );
    // We really are testing object creation, but this is a good proxy.
    $this->assertEquals('string_test', $info->name);
  }

  /**
   * The valid type strings set in CoveoFieldDataType.
   *
   * @return array
   *   A type string.
   */
  public static function providerTypes(): array {
    return [['LONG'], ['LONG_64'], ['DOUBLE'], ['DATE'], ['VECTOR'], ['STRING']];
  }

}
