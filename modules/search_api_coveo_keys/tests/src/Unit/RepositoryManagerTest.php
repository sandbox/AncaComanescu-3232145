<?php

declare(strict_types=1);

namespace Drupal\Tests\search_api_coveo_keys\Unit;

use Drupal\search_api_coveo_keys\RepositoryManager;
use Drupal\Tests\UnitTestCase;

/**
 * Test value retrieval from keys..
 *
 * @group search_api_coveo_keys
 */
final class RepositoryManagerTest extends UnitTestCase {

  /**
   * The instantiated class under test.
   *
   * @var \Drupal\search_api_coveo_keys\RepositoryManager
   */
  protected RepositoryManager $repository;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $key = $this->getMockBuilder('\Drupal\key\Entity\Key')
      ->setConstructorArgs([[], 'key'])
      ->onlyMethods(['getKeyValues'])
      ->getMock();
    $key->method('getKeyValues')
      ->willReturn(['value-key' => 'test']);
    $keyRepository = $this->createMock('\Drupal\key\KeyRepositoryInterface');
    $keyRepository->method('getKey')
      ->willReturn($key);
    $this->repository = new RepositoryManager($keyRepository);
  }

  /**
   * Tests the repository manager.
   *
   * @covers \Drupal\search_api_coveo_keys\RepositoryManager::getKeyValues
   */
  public function testKeyValues(): void {
    $value = $this->repository->getKeyValues('key');
    $this->assertEquals('test', $value['value-key']);
  }

}
