<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo_keys\Plugin\KeyType;

use Drupal\key\Plugin\KeyType\AuthenticationMultivalueKeyType;

/**
 * Key module plugin to define KeyType with fields for Coveo credentials'.
 *
 * @KeyType(
 *   id = "search_api_coveo",
 *   label = @Translation("Search API Coveo Keys"),
 *   description = @Translation("A key type to store confidential keys for Search API Coveo backend access. Store as JSON, using empty strings for fields that are not needed.:<br><pre>{<br>&quot;organization_id&quot;: &quot;organization_id value&quot;,<br>&quot;source_id&quot;: &quot;source_id value&quot;,<br>&quot;api_key&quot;: &quot;api_key value&quot;,<br>&quot;source_name&quot;: &quot;source_name value&quot;<br>}</pre>"),
 *   group = "authentication",
 *   key_value = {
 *     "plugin" = "textarea_field"
 *   },
 *   multivalue = {
 *     "enabled" = true,
 *     "fields" = {
 *       "organization_id" =  @Translation("Organization ID"),
 *       "source_id" = @Translation("Source ID"),
 *       "api_key" = @Translation("API Key"),
 *       "source_name" = @Translation("Source Name"),
 *     }
 *   }
 * )
 */
class SearchApiCoveoKey extends AuthenticationMultivalueKeyType {

}
