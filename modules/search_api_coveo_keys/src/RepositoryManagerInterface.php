<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo_keys;

/**
 * Provide an integration with Key module's repository.
 *
 * This allows us to use the Key service and have valid classes for
 * static analysis.
 */
interface RepositoryManagerInterface {

  /**
   * Get a key's values.
   *
   * @param string $storageKey
   *   The id of the key.
   *
   * @return array
   *   The values stored in the key.
   */
  public function getKeyValues(string $storageKey): array;

}
