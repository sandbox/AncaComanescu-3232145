<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo_keys;

use Drupal\key\Entity\Key;
use Drupal\key\KeyRepositoryInterface;

/**
 * An internal service to integrate with Key module.
 */
class RepositoryManager implements RepositoryManagerInterface {

  /**
   * Constructs a RepositoryManager object.
   */
  public function __construct(
    private readonly KeyRepositoryInterface $keyRepository,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function getKeyValues(string $storageKey): array {
    $value = [];
    $keyEntity = $this->keyRepository->getKey($storageKey);
    if ($keyEntity instanceof Key) {
      // A key was found in the repository.
      $value = $keyEntity->getKeyValues();
    }
    return $value;
  }

}
