<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo;

use Drupal\search_api_coveo\DataStructure\CoveoPlatformEndpoint;
use Drupal\search_api_coveo\DataStructure\CoveoPushApiEndpoint;

/**
 * Client context value object.
 */
class SearchApiCoveoClientContext {

  /**
   * The Coveo Field API Batch path stem.
   */
  const FIELD_BATCH = 'indexes/fields/batch';

  /**
   * The Coveo Field API load path stem.
   */
  const FIELD_LOAD = 'indexes/fields';

  /**
   * The Coveo Field API field listing path.
   */
  const FIELD_LIST = 'indexes/fields/search';

  /**
   * The Coveo Push API status path.
   *
   * @see https://platform.cloud.coveo.com/docs?urls.primaryName=PushAPI#/Source%20Status/post_organizations__organizationId__sources__sourceId__status
   */
  const PUSH_STATUS = 'status';

  /**
   * The Coveo Push API batch path.
   *
   * @see https://docs.coveo.com/en/12/api-reference/push-api#tag/Item/paths/~1organizations~1{organizationId}~1sources~1{sourceId}~1documents~1batch/put
   */
  const PUSH_BATCH = 'documents/batch';

  /**
   * The Coveo Push API delete by time path.
   *
   * @see https://platform.cloud.coveo.com/docs?urls.primaryName=PushAPI#/Item/delete_organizations__organizationId__sources__sourceId__documents_olderthan
   */
  const PUSH_DELETE_TIME = 'documents/olderthan';

  /**
   * The Coveo Push API delete by time path.
   *
   * @see https://platform.cloud.coveo.com/docs?urls.primaryName=PushAPI#/Item/delete_organizations__organizationId__sources__sourceId__documents
   */
  const PUSH_DELETE_ITEM = 'documents';

  /**
   * Push API Base URL.
   *
   * @var string
   */
  public readonly string $pushBase;
  /**
   * Default Headers.
   *
   * @var array
   */
  public readonly array $defaultHeaders;

  /**
   * Field API Base URL.
   *
   * @var string
   */
  public readonly string $fieldBase;

  /**
   * Administrative Base URL.
   *
   * @var string
   */
  public readonly string $adminBase;

  /**
   * Organization Status Base URL.
   *
   * @var string
   */
  public readonly string $organizationBase;

  /**
   * S3 Container Base URL.
   *
   * @var string
   *
   * @see https://docs.coveo.com/en/12/api-reference/push-api#tag/File-Container/paths/~1organizations~1{organizationId}~1files/post
   */
  public readonly string $containerBase;

  /**
   * Constructor.
   *
   * @param string $organizationID
   *   Organization (company) ID.
   * @param string $sourceID
   *   Push source ID.
   * @param string $apiKey
   *   Api key.
   * @param \Drupal\search_api_coveo\DataStructure\CoveoPushApiEndpoint $host
   *   Host.
   *
   * @throws \Exception
   */
  public function __construct(
    public readonly string $organizationID,
    public readonly string $sourceID,
    public readonly string $apiKey,
    CoveoPushApiEndpoint $host,
  ) {

    $this->pushBase = $host->value . '/organizations/' . $this->organizationID . '/sources/' . $this->sourceID . '/';

    $this->fieldBase = match($host) {
      CoveoPushApiEndpoint::Hipaa => CoveoPlatformEndpoint::Hipaa->value . '/rest/organizations/' . $this->organizationID . '/',
      default => CoveoPlatformEndpoint::Default->value . '/rest/organizations/' . $this->organizationID . '/',
    };

    $this->adminBase = match($host) {
      CoveoPushApiEndpoint::Hipaa => CoveoPlatformEndpoint::Hipaa->value . '/admin/',
      default => CoveoPlatformEndpoint::Default->value . '/admin/',
    };

    $this->organizationBase = $host->value . '/rest/organizations/';

    $this->containerBase = $host->value . '/organizations/' . $this->organizationID . '/';

    $this->defaultHeaders = [
      'Accept' => 'application/json',
      'Content-Type' => 'application/json',
      'Authorization' => 'Bearer ' . $this->apiKey,
    ];
  }

}
