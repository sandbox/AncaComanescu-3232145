<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo;

use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\search_api_coveo\DataStructure\CoveoPushApiEndpoint;

/**
 * Generates a client object for a context.
 */
final class SearchApiCoveoClientFactory implements SearchApiCoveoClientFactoryInterface {

  /**
   * Constructs a SearchApiCoveoClientFactory object.
   */
  public function __construct(
    private readonly SearchApiCoveoLoggerFactoryInterface $searchApiCoveoLoggers,
    private readonly ClientFactory $httpClientFactory,
    private readonly MessengerInterface $messenger,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function getClient(string $sourceID, string $organizationID, string $apiKey, CoveoPushApiEndpoint $host, string $serverId): SearchApiCoveoClientInterface {
    return new SearchApiCoveoClient(
      context: new SearchApiCoveoClientContext($organizationID, $sourceID, $apiKey, $host),
      clientFactory: $this->httpClientFactory,
      messenger: $this->messenger,
      logger: $this->searchApiCoveoLoggers->get($serverId)
    );
  }

}
