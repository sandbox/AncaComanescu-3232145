<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo;

use Drupal\search_api_coveo\DataStructure\CoveoDocument;
use Drupal\search_api_coveo\DataStructure\CoveoFieldInfo;
use Drupal\search_api_coveo\DataStructure\CoveoFieldInfoSet;
use Drupal\search_api_coveo\DataStructure\CoveoFieldOrigin;

/**
 * Entry point in the PHP API.
 */
interface SearchApiCoveoClientInterface {

  /**
   * Start the batch.
   */
  public function startBatch(): bool;

  /**
   * Add CoveoDocument to batch.
   *
   * @param \Drupal\search_api_coveo\DataStructure\CoveoDocument $document
   *   The coveo document.
   *
   * @return bool
   *   Was the document added?
   */
  public function addDocument(CoveoDocument $document): bool;

  /**
   * End batch.
   *
   * @return bool
   *   Did the batch successfully end?
   */
  public function endBatch(): bool;

  /**
   * This function deletes the index content.
   *
   * Settings and index specific API keys are kept untouched.
   *
   * @return bool
   *   Deletion succeeded.
   *
   * @throws \Exception
   */
  public function deleteOlderThan(): bool;

  /**
   * Delete Single CoveoDocument.
   *
   * @param string $documentId
   *   Coveo Document Id.
   *
   * @return bool
   *   The result of the deleteDocument call.
   */
  public function deleteItem(string $documentId): bool;

  /**
   * Get COVEO Organizations.
   *
   * @return array
   *   List of COVEO organizations.
   */
  public function getOrganizations(): array;

  /**
   * Saves a field into coveo.
   *
   * @param \Drupal\search_api_coveo\DataStructure\CoveoFieldInfoSet $bulk
   *   A set of CoveoFieldInfo to bulk create fields.
   *
   * @return bool
   *   If the operation was successful.
   *
   * @see https://docs.coveo.com/en/8/api-reference/field-api#tag/Fields/operation/createFields
   */
  public function createFields(CoveoFieldInfoSet $bulk): bool;

  /**
   * Loads the current USER type fields from coveo.
   *
   * @return \Drupal\search_api_coveo\DataStructure\CoveoFieldInfoSet
   *   The set of fields found.
   *
   * @see https://docs.coveo.com/en/8/api-reference/field-api#tag/Fields/operation/getFields
   */
  public function loadUserFields(): CoveoFieldInfoSet;

  /**
   * Retrieves a filtered and paginated list of fields.
   *
   * List is retrieved from the index of the target Coveo Cloud organization.
   *
   * @param array $parameters
   *   Parameters to add to the POST request.
   * @param \Drupal\search_api_coveo\DataStructure\CoveoFieldOrigin $origin
   *   The origin of the fields to list. "ALL" "SYSTEM" "USER".
   *
   * @return \Drupal\search_api_coveo\DataStructure\CoveoFieldInfoSet
   *   Return all the fields.
   *
   * @see https://docs.coveo.com/en/8/cloud-v2-api-reference/field-api#operation/getFieldsUsingPOST_6
   */
  public function listFields(array $parameters, CoveoFieldOrigin $origin = CoveoFieldOrigin::User): CoveoFieldInfoSet;

  /**
   * Get field by field_id/coveo name.
   *
   * @return \Drupal\search_api_coveo\DataStructure\CoveoFieldInfo|false
   *   Array of field info.
   *
   * @see https://docs.coveo.com/en/8/cloud-v2-api-reference/field-api#operation/updateFieldsUsingPUT_2
   */
  public function loadField(string $field_id): CoveoFieldInfo|false;

  /**
   * Get coveo system fields.
   *
   * @return \Drupal\search_api_coveo\DataStructure\CoveoFieldInfoSet
   *   The system fields as a set..
   *
   * @see https://docs.coveo.com/en/8/api-reference/field-api#tag/Fields/operation/getFields
   */
  public function loadSystemFields(): CoveoFieldInfoSet;

  /**
   * Updates one or more field in coveo.
   *
   * @param \Drupal\search_api_coveo\DataStructure\CoveoFieldInfoSet $bulk
   *   A set of CoveoFieldInfo to bulk create fields.
   *
   * @return array|false
   *   The response.
   *
   * @see https://docs.coveo.com/en/8/cloud-v2-api-reference/field-api#operation/updateFieldsUsingPUT_2
   */
  public function updateFields(CoveoFieldInfoSet $bulk): array|false;

  /**
   * Deletes fields from coveo.
   *
   * @param \Drupal\search_api_coveo\DataStructure\CoveoFieldInfoSet $bulk
   *   A bulk group to delete.
   *
   * @return bool
   *   If the operation was successful.
   *
   * @see https://docs.coveo.com/en/8/api-reference/field-api#tag/Fields/operation/removeField
   */
  public function deleteFields(CoveoFieldInfoSet $bulk): bool;

}
