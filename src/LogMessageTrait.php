<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo;

use Drupal\Core\Logger\RfcLogLevel;

/**
 * Provides a helper method for logging messages..
 */
trait LogMessageTrait {

  /**
   * Log message with severity.
   *
   * @param string $message
   *   The message to display in the log, which can use variables.
   * @param array $variables
   *   (optional) Array of variables to replace in the message when it is
   *   displayed, or NULL if the message should not be translated. The normal
   *   patterns for translation variables can be used.
   * @param int $severity
   *   (optional) The severity of the message, as per RFC 3164.
   * @param string|null $link
   *   (optional) A link to associate with the message, if any.
   */
  protected function logMessage($message, array $variables = [], $severity = RfcLogLevel::ERROR, $link = NULL) {
    if ($link) {
      $variables['link'] = $link;
    }

    $this->getLogger()->log($severity, $message, $variables);
  }

}
