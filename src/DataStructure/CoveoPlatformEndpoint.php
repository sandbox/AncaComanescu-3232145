<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo\DataStructure;

/**
 * The base URLs used in the Coveo .
 */
enum CoveoPlatformEndpoint: string {
  case Default = 'http://platform.cloud.coveo.com';
  case Hipaa = 'https://platformhipaa.cloud.coveo.com';
}
