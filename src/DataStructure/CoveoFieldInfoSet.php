<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo\DataStructure;

use Drupal\data_structures\DataStructure\Set;
use Drupal\data_structures\DataStructure\SetInterface;

/**
 * Provides a typed set of CoveoFieldInfo objects.
 */
class CoveoFieldInfoSet extends Set {

  /**
   * {@inheritdoc}
   */
  public function add(...$values): void {
    foreach ($values as $value) {
      if (!$this->has($value)) {
        $this->pushValue($value);
      }
    }
  }

  /**
   * A type safe helper function to ensure that all members are CoveoFieldInfo.
   *
   * @param \Drupal\search_api_coveo\DataStructure\CoveoFieldInfo $value
   *   The field info value.
   */
  protected function pushValue(CoveoFieldInfo $value): void {
    $this->members[] = $value;
  }

  // phpcs:disable Generic.CodeAnalysis.UselessOverridingMethod.Found

  /**
   * {@inheritdoc}
   */
  public function union(SetInterface $set): CoveoFieldInfoSet {
    return new static(parent::union($set));
  }

  /**
   * {@inheritdoc}
   */
  public function intersect(SetInterface $set): CoveoFieldInfoSet {
    return new static(parent::intersect($set));
  }

  /**
   * {@inheritdoc}
   */
  public function difference(SetInterface $set): CoveoFieldInfoSet {
    return new static(parent::difference($set));
  }

  /**
   * {@inheritdoc}
   */
  public function symDifference(SetInterface $set): CoveoFieldInfoSet {
    return new static(parent::symDifference($set));
  }

  /**
   * {@inheritdoc}
   */
  public function map(callable $callable): CoveoFieldInfoSet {
    return new static(parent::map($callable));
  }

  /**
   * {@inheritdoc}
   */
  public function filter(callable $callable): CoveoFieldInfoSet {
    return new static(parent::filter($callable));
  }

  /**
   * Get all the fieldIds (names) in the Set.
   *
   * @return string[]
   *   All the field names.
   */
  public function getFieldIds(): array {
    $names = [];
    foreach ($this->members as $member) {
      $names[] = $member->name;
    }
    return $names;
  }

}
