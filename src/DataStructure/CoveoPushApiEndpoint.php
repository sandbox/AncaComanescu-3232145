<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo\DataStructure;

/**
 * The URLs used in the Coveo Push API.
 */
enum CoveoPushApiEndpoint: string {
  case Dev = 'https://api-dev.cloud.coveo.com/push/v1';
  case Qa = 'https://api-qa.cloud.coveo.com/push/v1';
  case Prod = 'https://api.cloud.coveo.com/push/v1';
  case Hipaa = 'https://apihipaa.cloud.coveo.com/push/v1';
}
