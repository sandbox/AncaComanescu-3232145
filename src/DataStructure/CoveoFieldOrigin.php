<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo\DataStructure;

/**
 * Allowed values for the Coveo field origin property.
 *
 * @see https://docs.coveo.com/en/8/api-reference/field-api#tag/Fields/operation/getFields
 */
enum CoveoFieldOrigin: string {
  case All = 'ALL';
  case System = 'SYSTEM';
  case User = 'USER';
}
