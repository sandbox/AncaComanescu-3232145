<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo\DataStructure;

/**
 * An immutable value map class with readonly properties.
 *
 * @see https://docs.coveo.com/en/12/api-reference/push-api#tag/File-Container/paths/~1organizations~1{organizationId}~1files/post
 */
final class CoveoS3FileData {

  /**
   * A value map class for S3 file data used in batch pushes..
   *
   * @param string $uploadUri
   *   The upload Uri returned from the push api.
   * @param string[] $requiredHeaders
   *   Required headers returned from the push api.
   * @param string $fileId
   *   The file id returned from the push api.
   */
  public function __construct(
    public readonly string $uploadUri,
    public readonly array $requiredHeaders,
    public readonly string $fileId,
  ) {}

}
