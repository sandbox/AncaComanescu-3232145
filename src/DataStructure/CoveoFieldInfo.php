<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo\DataStructure;

use Drupal\data_structures\DataStructure\EqualityInterface;

/**
 * A value map class for field info.
 *
 * This value object both documents and validates Coveo field configuration
 * data. Many of these values are defined conditionally in the Coveo
 * documentation based on other values in the configuration.
 *
 * @see https://docs.coveo.com/en/8/api-reference/field-api#tag/Fields/operation/createField
 * @see https://docs.coveo.com/en/149/index-content/available-boolean-field-options
 */
final class CoveoFieldInfo implements EqualityInterface, \JsonSerializable {

  /**
   * A validated coveo field data type.
   *
   * Evolving coding standard discussion found at:
   *
   * @see https://www.drupal.org/project/coding_standards/issues/3339746
   */
  public CoveoFieldDataType $type;

  /**
   * The strict identifier of the field.
   *
   * This value is validated at construction and therefore is set to
   * readonly.
   *
   * @var string
   */
  public readonly string $name;

  /**
   * Prepare a data object for a Coveo field..
   *
   * @param string $name
   *   The name of the field, also referred to as the field identifier,
   *   or `fieldId`.
   * @param bool|null $automaticFacetsCandidate
   *   Whether this field is to be considered by the facet generator feature.
   * @param string|null $dateFormat
   *   A regex limiting the date format.
   * @param string|null $description
   *   A description of the field.
   * @param bool|null $facet
   *   Whether `Group By` operations and `Facet` requests can be performed on
   *   the field.
   * @param bool|null $hierarchicalFacet
   *   Whether the field contains multiple values that form a hierarchy.
   * @param bool|null $includeInQuery
   *   Whether this field can be referred to in Coveo Cloud query syntax
   *   expressions.
   * @param bool|null $includeInResults
   *   Whether the field can be included in the raw property of query results.
   * @param bool|null $keepAccentsDisplayValueFacet
   *   Whether field values with differing accents should be considered distinct
   *   facet values.
   * @param bool|null $keyValue
   *   Whether the field is a dictionary field, which contains mappings of keys
   *   to values instead of a single value.
   * @param string|null $label
   *   A displayable label for the field when used by the facet generator.
   * @param bool|null $mergeWithLexicon
   *   Whether the field is free text searchable.
   * @param bool|null $multiValueFacet
   *   Whether the field contains multiple values.
   * @param string|null $multiValueFacetTokenizers
   *   The character to use as a value separator, if the field contains multiple
   *   values.
   * @param bool|null $ranking
   *   Whether to use the field in result ranking calculation only.
   * @param bool|null $smartDateFacet
   *   Whether to transform date and time string values into semicolon separated
   *   number of days/weeks/months/quarters/years since January 1st, 1900.
   * @param bool|null $sort
   *   Whether query results can be sorted based on the value of the field.
   * @param bool|null $stemming
   *   Whether to allow an item to match the query when the value of this field
   *   for this item stems from the same root as one of the query expression
   *   keywords.
   * @param bool|null $system
   *   Whether the field is a standard Coveo field. May only be set to true by
   *   Coveo.
   * @param string $type
   *   The data type of the field. Validated with CoveoFieldDataType.
   * @param bool|null $useCacheForComputedFacet
   *   Whether to keep computed field data in memory.
   * @param bool|null $useCacheForNestedQuery
   *   Whether to keep the data required to perform nested queries in memory.
   * @param bool|null $useCacheForNumericQuery
   *   Whether to keep the data required to execute operations on numeric and
   *   date fields in memory.
   * @param bool|null $useCacheForSort
   *   Whether to keep the entire field in memory for fast sorting.
   *
   * @see https://docs.coveo.com/en/162/#about-the-date-field-type
   */
  public function __construct(
    string $name,
    public ?bool $automaticFacetsCandidate = NULL,
    public ?string $dateFormat = NULL,
    public ?string $description = NULL,
    public ?bool $facet = NULL,
    public ?bool $hierarchicalFacet = NULL,
    public ?bool $includeInQuery = NULL,
    public ?bool $includeInResults = NULL,
    public ?bool $keepAccentsDisplayValueFacet = NULL,
    public ?bool $keyValue = NULL,
    public ?string $label = NULL,
    public ?bool $mergeWithLexicon = NULL,
    public ?bool $multiValueFacet = NULL,
    public ?string $multiValueFacetTokenizers = NULL,
    public ?bool $ranking = NULL,
    public ?bool $smartDateFacet = NULL,
    public ?bool $sort = NULL,
    public ?bool $stemming = NULL,
    public ?bool $system = NULL,
    string $type = 'STRING',
    public ?bool $useCacheForComputedFacet = NULL,
    public ?bool $useCacheForNestedQuery = NULL,
    public ?bool $useCacheForNumericQuery = NULL,
    public ?bool $useCacheForSort = NULL,
  ) {
    if (preg_match('#^([a-z][a-z0-9_]{0,254})$#', $name) !== 1) {
      throw new \ValueError("$name is not a valid Coveo field name.");
    }
    $this->name = $name;
    // Use the enum to validate the type string.
    $this->type = CoveoFieldDataType::from($type);
  }

  /**
   * Transform the value object to an associative array.
   *
   * Filters out all null values.
   *
   * @return array
   *   The filtered array.
   */
  public function toArray(): array {
    $values = [
      'automaticFacetsCandidate' => $this->automaticFacetsCandidate ,
      'dateFormat' => $this->dateFormat,
      'description' => $this->description,
      'facet' => $this->facet,
      'hierarchicalFacet' => $this->hierarchicalFacet,
      'includeInResults' => $this->includeInResults,
      'keepAccentsDisplayValueFacet' => $this->keepAccentsDisplayValueFacet,
      'keyValue' => $this->keyValue,
      'label' => $this->label,
      'mergeWithLexicon' => $this->mergeWithLexicon,
      'multiValueFacet' => $this->multiValueFacet,
      'multiValueFacetTokenizers' => $this->multiValueFacetTokenizers,
      'name' => $this->name,
      'ranking' => $this->ranking,
      'smartDateFacet' => $this->smartDateFacet,
      'sort' => $this->sort,
      'stemming' => $this->stemming,
      'system' => $this->system,
      'type' => $this->type,
      'useCacheForComputedFacet' => $this->useCacheForComputedFacet,
      'useCacheForNestedQuery' => $this->useCacheForNestedQuery,
      'useCacheForNumericQuery' => $this->useCacheForNumericQuery,
      'useCacheForSort' => $this->useCacheForSort,
    ];
    // Reduce the array to keys with explicit values.
    $not_null = fn($value) => !is_null($value);
    return array_filter($values, $not_null);
  }

  /**
   * Set all values to their defaults.
   *
   * @see https://docs.coveo.com/en/8/api-reference/field-api#tag/Fields/operation/updateFields
   */
  public function setDefaults(): void {
    // $this->dateFormat must be explicitly set.
    $this->description = $this->description ?? '';
    $this->facet = $this->facet ?? $this->type !== CoveoFieldDataType::String;
    if ($this->type === CoveoFieldDataType::String) {
      $this->hierarchicalFacet = $this->hierarchicalFacet ?? FALSE;
    }
    $this->includeInResults = $this->includeInResults ?? TRUE;
    $this->keepAccentsDisplayValueFacet = $this->keepAccentsDisplayValueFacet ?? FALSE;
    $this->keyValue = $this->keyValue ?? FALSE;
    $this->label = $this->label ?? '';
    if ($this->type === CoveoFieldDataType::String) {
      $this->mergeWithLexicon = $this->mergeWithLexicon ?? FALSE;
    }
    if ($this->type === CoveoFieldDataType::String) {
      $this->multiValueFacet = $this->multiValueFacet ?? FALSE;
    }
    $this->automaticFacetsCandidate = $this->automaticFacetsCandidate ?? $this->facet || $this->multiValueFacet;
    // $this->multiValueFacetTokenizers must be explicitly set.
    if ($this->type === CoveoFieldDataType::String) {
      $this->ranking = $this->ranking ?? FALSE;
    }
    if ($this->type === CoveoFieldDataType::Date) {
      $this->smartDateFacet = $this->smartDateFacet ?? FALSE;
    }
    $this->sort = $this->sort ?? $this->type !== CoveoFieldDataType::String;
    if ($this->type === CoveoFieldDataType::String) {
      $this->stemming = $this->stemming ?? FALSE;
    }
    $this->system = $this->system ?? FALSE;
    if ($this->type !== CoveoFieldDataType::String) {
      $this->useCacheForComputedFacet = $this->useCacheForComputedFacet ?? FALSE;
    }
    if ($this->facet || $this->multiValueFacet) {
      $this->useCacheForNestedQuery = $this->useCacheForNestedQuery ?? FALSE;
    }
    if ($this->type !== CoveoFieldDataType::String) {
      $this->useCacheForNumericQuery = $this->useCacheForNumericQuery ?? FALSE;
    }
    $this->useCacheForSort = $this->useCacheForSort ?? FALSE;
  }

  /**
   * The name is a unique identifier in Coveo fields.
   *
   * @return string
   *   The field name.
   */
  public function hash(): string {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function equalTo(EqualityInterface $instance): bool {
    return self::class == $instance::class && $this->name === $instance->name;
  }

  /**
   * {@inheritdoc}
   */
  public function jsonSerialize(): array {
    return $this->toArray();
  }

}
