<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo\DataStructure;

/**
 * Coveo field data types.
 *
 * @see https://docs.coveo.com/en/2036/index-content/about-fields#field-types
 * @see https://docs.coveo.com/en/8/api-reference/field-api#tag/Fields/operation/createField
 */
enum CoveoFieldDataType: string {
  case Long = 'LONG';
  case Long64 = 'LONG_64';
  case Double = 'DOUBLE';
  case Date = 'DATE';
  case String = 'STRING';
  case Vector = 'VECTOR';
}
