<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo;

use Drupal\Core\Logger\LoggerChannel;

/**
 * A Drupal logger with logging threshold.
 */
class SearchApiCoveoLoggerChannel extends LoggerChannel {

  /**
   * Created in SearchApiCoveoLoggerFactory.
   *
   * @param int $logThreshold
   *   The maximum RFC 5424 log level.
   */
  public function __construct(
    protected int $logThreshold,
  ) {
    parent::__construct('search_api_coveo');
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, \Stringable|string $message, array $context = []): void {
    if (is_string($level)) {
      // Convert to integer equivalent for consistency with RFC 5424.
      $level = $this->levelTranslation[$level];
    }
    if ($level > $this->logThreshold) {
      // Do not log.
      return;
    }
    parent::log($level, $message, $context);
  }

}
