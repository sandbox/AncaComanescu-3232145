<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo;

use Drupal\Core\State\StateInterface;
use Drupal\search_api_coveo_keys\RepositoryManagerInterface;

/**
 * Implements a credential service to support multiple providers.
 */
class CredentialProvider implements CredentialProviderInterface {

  /**
   * Key module service conditionally injected.
   *
   * @var \Drupal\search_api_coveo_keys\RepositoryManagerInterface
   */
  protected RepositoryManagerInterface $keyRepository;

  /**
   * Constructor for service injection.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   Injected service.
   */
  public function __construct(protected StateInterface $state) {}

  /**
   * {@inheritdoc}
   */
  public function setKeyRepository(RepositoryManagerInterface $repository): void {
    $this->keyRepository = $repository;
  }

  /**
   * {@inheritdoc}
   */
  public function additionalProviders(): bool {
    return !empty($this->keyRepository);
  }

  /**
   * {@inheritdoc}
   */
  public function getCredentials(array $config): array|string {
    $credentials = [];
    $provider = $this->getProvider($config);
    if (empty($provider)) {
      return $credentials;
    }
    $credentials = match ($provider) {
      'key' => $this->keyRepository->getKeyValues($this->getStorageKey($config)),
      default => $this->state->get($this->getStorageKey($config)) ?? [],
    };

    return $credentials;
  }

  /**
   * Helper function to get the string used as a storage key by the provider.
   *
   * @param array $config
   *   The plugin config.
   *
   * @return string
   *   The storage key.
   */
  protected function getStorageKey(array $config): string {
    return $config['storage_key'] ?? '';
  }

  /**
   * Helper function to get string identifying the credential provider.
   *
   * @param array $config
   *   The plugin config.
   *
   * @return string
   *   The provider id.
   */
  protected function getProvider(array $config): string {
    return $config['credential_provider'] ?? '';
  }

}
