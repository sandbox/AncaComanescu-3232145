<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This factory looks up the log threshold and returns a logging channel.
 */
class SearchApiCoveoLoggerFactory implements SearchApiCoveoLoggerFactoryInterface {

  /**
   * A memory cache of configured logger channels.
   *
   * @var SearchApiCoveoLoggerChannel[]
   */
  protected array $channels = [];

  /**
   * An array of arrays of \Psr\Log\LoggerInterface keyed by priority.
   *
   * @var array
   */
  protected $loggers = [];

  /**
   * Server storage service.
   */
  protected EntityStorageInterface $serverStorage;

  /**
   * Initialize the factory.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, protected ContainerInterface $container) {
    $this->serverStorage = $entityTypeManager->getStorage('search_api_server');
  }

  /**
   * {@inheritdoc}
   */
  public function get(string $serverId): SearchApiCoveoLoggerChannel {
    if (isset($this->channels[$serverId])) {
      return $this->channels[$serverId];
    }
    /** @var \Drupal\search_api\Entity\Server|null $server */
    $server = $this->serverStorage->load($serverId);
    $config = is_null($server) ? [] : $server->getBackendConfig();
    $threshold = $config['log_threshold'] ?? RfcLogLevel::WARNING;
    $channel = new SearchApiCoveoLoggerChannel($threshold);

    // Set the request_stack and current_user services.
    $channel->setRequestStack($this->container->get('request_stack'));
    $channel->setCurrentUser($this->container->get('current_user'));

    // Pass the loggers to the channel.
    $channel->setLoggers($this->loggers);
    // Cache the channel.
    $this->channels[$serverId] = $channel;
    return $this->channels[$serverId];
  }

  /**
   * Used by logging services to announce themselves to loggers.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger to add.
   * @param int $priority
   *   The priority the logger should use.
   */
  public function addLogger(LoggerInterface $logger, $priority = 0) {
    // Store it so we can pass it to potential new logger instances.
    $this->loggers[$priority][] = $logger;
    // Add the logger to already instantiated channels.
    foreach ($this->channels as $channel) {
      $channel->addLogger($logger, $priority);
    }
  }

}
