<?php

namespace Drupal\search_api_coveo\Plugin\search_api\processor;

use Drupal\Core\Security\DoTrustedCallbackTrait;
use Drupal\Core\Utility\CallableResolver;
use Drupal\Core\Utility\Error;
use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Allows adding custom tokenized text values to the index.
 *
 * @SearchApiProcessor(
 *   id = "coveo_calculated_value",
 *   label = @Translation("Calculated value"),
 *   description = @Translation("Allows adding callbacks to create calculate values and add them to the index."),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class CoveoCalculatedValue extends ProcessorPluginBase {

  use DoTrustedCallbackTrait;

  /**
   * The callable resolver.
   *
   * @var \Drupal\Core\Utility\CallableResolver
   */
  protected CallableResolver $callableResolver;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.factory')->get('search_api_coveo');
    $instance->callableResolver = $container->get('callable_resolver');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(?DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Calculated value'),
        'description' => $this->t('Add a calculated value to the index a callback.'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['coveo_calculated_value'] = new CoveoCalculatedValueProperty($definition);
    }

    return $properties;
  }

  /**
   * Assemble the calculated fields and apply their callbacks.
   *
   * @param \Drupal\search_api\Item\ItemInterface $item
   *   The item being indexed.
   *
   * @throws \Drupal\search_api\SearchApiException
   */
  public function addFieldValues(ItemInterface $item): void {
    // Get all of the "custom_value" fields on this item.
    $fields = $this->getFieldsHelper()
      ->filterForPropertyPath($item->getFields(), NULL, 'coveo_calculated_value');
    // Pass the original object to the callable.
    $data['object'] = $item->getOriginalObject()->getValue();

    // Iterate the collected fields and apply their callbacks.
    foreach ($fields as $field) {
      try {
        $config = $field->getConfiguration();
        $callback = $config['callable'] ?? NULL;
        if ($callback === '' || is_null($callback)) {
          continue;
        }
        $callable = $this->callableResolver->getCallableFromDefinition($callback);
        $value = $this->doTrustedCallback($callable, $data, 'CoveoCalculatedValue callbacks must be methods annotated with Drupal\Core\Security\Attribute\TrustedCallback');
        $field->addValue($value);
      }
      catch (\Exception $e) {
        $id = $field->getFieldIdentifier();
        $message = 'Exception: ' . $e->getMessage();
        $message .= " Encountered while executing $callback on field $id";
        Error::logException($this->logger, $e, $message);
        continue;
      }
    }
  }

}
