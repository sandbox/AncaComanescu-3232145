<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo\Plugin\search_api\processor;

use Drupal\search_api\Processor\FieldsProcessorPluginBase;
use Drupal\Component\Utility\Xss;

/**
 * Filters fields through \Drupal\Component\Utility\Xss::filterAdmin().
 *
 * A simplified implementation based on HtmlFilter.
 *
 * @SearchApiProcessor(
 *   id = "coveo_xss_admin_filter",
 *   label = @Translation("XSS Admin filter"),
 *   description = @Translation("Allows all tags that can be used inside an HTML body, save for scripts and styles."),
 *   stages = {
 *     "pre_index_save" = 0,
 *     "preprocess_index" = -15,
 *     "preprocess_query" = -15,
 *   }
 * )
 */
class CoveoXssAdmin extends FieldsProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function process(&$value) {
    $value = Xss::filterAdmin($value);
  }

}
