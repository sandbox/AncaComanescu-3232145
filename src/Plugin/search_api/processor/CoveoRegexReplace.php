<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo\Plugin\search_api\processor;

use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api\Processor\FieldsProcessorPluginBase;

/**
 * Filters fields through \Drupal\Component\Utility\Xss::filterAdmin().
 *
 * A simplified implementation based on HtmlFilter.
 *
 * @SearchApiProcessor(
 *   id = "coveo_regex_filter",
 *   label = @Translation("Regex filter"),
 *   description = @Translation("Finds and replaces using regular expressions"),
 *   stages = {
 *     "pre_index_save" = 0,
 *     "preprocess_index" = -15,
 *     "preprocess_query" = -15,
 *   }
 * )
 */
class CoveoRegexReplace extends FieldsProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = parent::defaultConfiguration();
    $configuration += [
      'patterns' => [],
    ];

    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['patterns'] = [
      '#title' => $this->t('Enter patterns and replacements'),
      '#type' => 'textarea',
      '#rows' => 24,
      '#required' => TRUE,
      '#default_value' => Yaml::encode($this->configuration['patterns']),
      '#description' => $this->t('Enter patterns and replacements in YAML format.  See the example below for more detail.'),
    ];
    $form['example'] = [
      '#type' => 'details',
      '#title' => $this->t('Example'),
    ];
    $exampleYaml = <<<EXAMPLE
- pattern: '# (data-[a-z-_]+|class|id)="[^"]+"#'
  replacement: ''
- pattern: '#foo|bar#'
  replacement: 'baz'
EXAMPLE;
    $form['example']['yaml'] = [
      '#type' => 'inline_template',
      '#template' => "<pre><code>{{ yaml }}</code></pre>",
      '#context' => [
        'yaml' => $exampleYaml,
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    $yaml = $form_state->getValue('patterns');
    try {
      Yaml::decode($yaml);
    }
    catch (InvalidDataTypeException $e) {
      $form_state->setErrorByName('patterns', $this->t('The import failed with the following message: %message', ['%message' => $e->getMessage()]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $values['patterns'] = Yaml::decode($values['patterns']);
    $this->setConfiguration($values);
  }

  /**
   * {@inheritdoc}
   */
  protected function process(&$value) {
    $replacementSet = $this->configuration['patterns'];
    foreach ($replacementSet as $pair) {
      $value = preg_replace($pair['pattern'], $pair['replacement'], $value);
    }
  }

}
