<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo\Plugin\search_api\backend;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\search_api\Backend\BackendPluginBase;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Item\FieldInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Plugin\search_api\data_type\value\TextValue;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api_coveo\CredentialProvider;
use Drupal\search_api_coveo\DataStructure\CoveoDocument;
use Drupal\search_api_coveo\DataStructure\CoveoFieldInfo;
use Drupal\search_api_coveo\DataStructure\CoveoFieldInfoSet;
use Drupal\search_api_coveo\DataStructure\CoveoPushApiEndpoint;
use Drupal\search_api_coveo\LogMessageTrait;
use Drupal\search_api_coveo\SearchApiCoveoClientInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Indexes items using Coveo Search.
 *
 * @SearchApiBackend(
 *   id = "search_api_coveo",
 *   label = @Translation("Coveo"),
 *   description = @Translation("Index items using a Coveo Search.")
 * )
 */
class SearchApiCoveoBackend extends BackendPluginBase implements PluginFormInterface {

  use PluginFormTrait;
  use LogMessageTrait;

  const CONTENT_FIELD_ID = 'rendered_item';
  const DOCUMENT_ID = 'documentId';
  const URL_FIELD_ID = 'clickableUri';
  const TITLE = 'title';

  /**
   * A connection to the Coveo server.
   */
  protected SearchApiCoveoClientInterface $coveoClient;

  /**
   * Storage for credentials retrieved from credential service.
   */
  private array $credentials;

  /**
   * Injected credential service.
   */
  protected CredentialProvider $credentialService;

  /**
   * Injected service.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Cache of field storage configuration associated with indexes.
   */
  protected array $fieldStorageConfigs = [];

  /**
   * Injected service.
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The module handler.
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The state service.
   */
  protected StateInterface $state;

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return \Drupal\search_api_coveo\Plugin\search_api\backend\SearchApiCoveoBackend
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): SearchApiCoveoBackend {
    /** @var \Drupal\search_api_coveo\Plugin\search_api\backend\SearchApiCoveoBackend $backend */
    $backend = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $backend->state = $container->get('state');

    /** @var \Drupal\Core\Extension\ModuleHandlerInterface $module_handler */
    $module_handler = $container->get('module_handler');
    $backend->setModuleHandler($module_handler);
    /** @var \Drupal\search_api_coveo\SearchApiCoveoLoggerFactoryInterface $loggerFactory */
    $loggerFactory = $container->get('search_api_coveo.loggers');
    $backend->setLogger($loggerFactory->get($backend->getServer()->id() ?? ''));
    $backend->entityTypeManager = $container->get('entity_type.manager');
    $backend->languageManager = $container->get('language_manager');
    try {
      $backend->credentialService = $container->get('search_api_coveo.credentials');
      $backend->credentials = $backend->credentialService->getCredentials($backend->getConfiguration());
    }
    catch (\Exception $e) {
      $backend->credentials = [
        'organization_id' => NULL,
        'source_id' => NULL,
        'api_key' => NULL,
        'source_name' => NULL,
      ];
    }
    if (!empty($backend->credentials['source_id'])) {
      /** @var \Drupal\search_api_coveo\SearchApiCoveoClientFactoryInterface $clientFactory */
      $clientFactory = $container->get('search_api_coveo.client_factory');
      $backend->coveoClient = $clientFactory->getClient(
        sourceID: $backend->credentials['source_id'],
        organizationID: $backend->credentials['organization_id'],
        apiKey: $backend->credentials['api_key'],
        host: $backend->getApiEndpoint(),
        serverId: empty($backend->serverId) ? '' : $backend->serverId
      );
    }
    return $backend;
  }

  /**
   * Sets the module handler to use for this plugin.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to use for this plugin.
   *
   * @return $this
   */
  public function setModuleHandler(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
    return $this;
  }

  /**
   * Returns the CoveoSearch client.
   *
   * @return \Drupal\search_api_coveo\SearchApiCoveoClientInterface
   *   The Coveo instance object.
   */
  public function getCoveoClient() {
    return $this->coveoClient;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'credential_provider' => '',
      'storage_key' => '',
      'push_api_endpoint' => '',
      'log_threshold' => RfcLogLevel::WARNING,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['push_api_endpoint'] = [
      '#type' => 'select',
      '#title' => $this->t('Push API URL'),
      '#label' => $this->t('Push API URL'),
      '#description' => $this->t('Push API URL.'),
      '#default_value' => $this->configuration['push_api_endpoint'],
      '#required' => TRUE,
      '#options' => [
        CoveoPushApiEndpoint::Dev->value => $this->t('Dev (api-dev.cloud.coveo.com)'),
        CoveoPushApiEndpoint::Hipaa->value => $this->t('Hipaa (apihipaa.cloud.coveo.com)'),
        CoveoPushApiEndpoint::Prod->value => $this->t('Prod (api.cloud.coveo.com)'),
        CoveoPushApiEndpoint::Qa->value => $this->t('Qa (api-qa.cloud.coveo.com)'),
      ],
    ];
    $form['log_threshold'] = [
      '#type' => 'select',
      '#title' => $this->t('Logging Threshold'),
      '#description' => $this->t('The highest level that will be sent to the log.'),
      '#default_value' => $this->configuration['log_threshold'],
      '#options' => RfcLogLevel::getLevels(),
    ];
    /*
    $organizations = $this->getOrganizations();
    $form['organizations'] = [
    '#type' => 'select',
    '#label' => $this->t('Organizations'),
    '#title' => $this->t('Organization'),
    '#description' => $this
    ->t('The list of organization from your Coveo subscription.'),
    '#default_value' => $this->configuration['organization_name'],
    '#required' => TRUE,
    options' => [],
    '#options' => array_combine($organizations, $organizations),
    ];
     */
    $form['credential_provider'] = [
      '#type' => 'hidden',
      '#value' => 'search_api_coveo',

    ];
    $have_additional_providers = $this->credentialService->additionalProviders();
    $form['search_api_coveo'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Stored locally'),
    ];
    $form['search_api_coveo']['organization_id'] = [
      '#type' => 'textfield',
      '#label' => $this->t('Organization ID'),
      '#title' => $this->t('Organization ID'),
      '#description' => $this->t('The organization ID from your Coveo subscription.'),
      '#default_value' => $this->credentials['organization_id'] ?? '',
      '#required' => !$have_additional_providers,
    ];
    $form['search_api_coveo']['source_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Source ID'),
      '#description' => $this->t('The Push Source ID.'),
      '#default_value' => $this->credentials['source_id'] ?? '',
      '#required' => !$have_additional_providers,
    ];
    $form['search_api_coveo']['source_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Source name'),
      '#description' => $this->t('The Push Source Name.'),
      '#default_value' => $this->credentials['source_name'] ?? '',
      '#required' => !$have_additional_providers,
    ];
    $form['search_api_coveo']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('The API key from your Coveo subscription.'),
      '#default_value' => $this->credentials['api_key'] ?? '',
      '#required' => !$have_additional_providers,
    ];
    // If Key module or some other future additional provider is available:
    if ($have_additional_providers) {
      $this->expandedProviderOptions($form);
    }
    return $form;
  }

  /**
   * Implements the form submission.
   *
   * @param array $form
   *   The render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $provider = $values['credential_provider'];
    $credentials = $values[$provider];
    array_walk($credentials, function (&$value) {
      $value = trim($value);
    });
    $key = 'search_api_coveo.credentials.' . $this->getPluginId();
    if ($provider == 'key') {
      $key = $credentials['id'];
    }
    elseif ($provider == 'search_api_coveo') {
      $this->state->set($key, $credentials);
    }
    $configuration = [
      'credential_provider' => $provider,
      'storage_key' => $key,
      'push_api_endpoint' => $values['push_api_endpoint'],
      'log_threshold' => $values['log_threshold'],
    ];
    $this->setConfiguration($configuration);
  }

  /**
   * Helper method to build the credential provider elements of the form.
   *
   * Only needed if we have more than one provider.  Currently supporting
   * search_api_coveo controlled local storage & Key module controlled optional
   * storage.
   *
   * @param array $form
   *   The modified configuration form.
   */
  protected function expandedProviderOptions(array &$form) {
    $provider = $this->getCredentialProvider();
    // Provide selectors for the api key credential provider.
    $form['credential_provider'] = [
      '#type' => 'select',
      '#title' => $this->t('Credential provider'),
      '#default_value' => empty($provider) ? 'search_api_coveo' : $provider,
      '#options' => [
        'search_api_coveo' => $this->t('Local storage'),
        'key' => $this->t('Key module'),
      ],
      '#attributes' => [
        'data-states-selector' => 'provider',
      ],
    ];
    $form['search_api_coveo']['#states'] = [
      'required' => [
        ':input[data-states-selector="provider"]' => ['value' => 'search_api_coveo'],
      ],
      'visible' => [
        ':input[data-states-selector="provider"]' => ['value' => 'search_api_coveo'],
      ],
      'enabled' => [
        ':input[data-states-selector="provider"]' => ['value' => 'search_api_coveo'],
      ],
    ];
    $key_id = $provider == 'key' ? $this->getStorageKey() : '';
    $form['key'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Managed by the Key module'),
      '#states' => [
        'required' => [
          ':input[data-states-selector="provider"]' => ['value' => 'key'],
        ],
        'visible' => [
          ':input[data-states-selector="provider"]' => ['value' => 'key'],
        ],
        'enabled' => [
          ':input[data-states-selector="provider"]' => ['value' => 'key'],
        ],
      ],
      'id' => [
        '#type' => 'key_select',
        '#title' => $this->t('Select a stored Key'),
        '#default_value' => $key_id,
        '#empty_option' => $this->t('- Please select -'),
        '#key_filters' => ['type' => 'search_api_coveo'],
        '#description' => $this->t('Select the key you have configured to hold the Coveo credentials.'),
      ],
    ];
  }

  /**
   * Gets the credential provider setting from configuration.
   *
   * @return string
   *   The configuration value
   */
  public function getCredentialProvider() {
    $configuration = $this->getConfiguration();
    return $configuration['credential_provider'] ?? NULL;
  }

  /**
   * Gets the storage key setting from configuration.
   *
   * @return string
   *   The configuration value
   */
  public function getStorageKey() {
    $configuration = $this->getConfiguration();
    return $configuration['storage_key'] ?? NULL;
  }

  /**
   * Gets the push_api_endpoint setting from configuration.
   *
   * @return string
   *   The configuration value
   */
  public function getPushEndpoint() {
    $configuration = $this->getConfiguration();
    return $configuration['push_api_endpoint'] ?? NULL;
  }

  /**
   * Gets the organization id from secure storage.
   *
   * @return string
   *   The stored value
   */
  public function getOrganizationId() {
    return $this->credentials['organization_id'] ?? '';
  }

  /**
   * Get the system fields from the Coveo instance.
   *
   * @return \Drupal\search_api_coveo\DataStructure\CoveoFieldInfoSet
   *   The system fields.
   */
  public function getCoveoSystemFields(): CoveoFieldInfoSet {
    return $this->coveoClient->loadSystemFields();
  }

  /**
   * Gets the source id from secure storage.
   *
   * @return string
   *   The stored value
   */
  public function getSourceId() {
    return $this->credentials['source_id'] ?? '';
  }

  /**
   * Gets the api key from secure storage.
   *
   * @return string
   *   The stored value
   */
  public function getApiKey() {
    return $this->credentials['api_key'] ?? '';
  }

  /**
   * Gets the source name from secure storage.
   *
   * @return string
   *   The stored value
   */
  public function getSourceName() {
    return $this->credentials['source_name'] ?? '';
  }

  /**
   * Converts a stored configuration string to the endpoint.
   *
   * @return \Drupal\search_api_coveo\DataStructure\CoveoPushApiEndpoint
   *   The endpoint enum from the config, defaults to CoveoPushApiEndpoint::Dev.
   */
  public function getApiEndpoint(): CoveoPushApiEndpoint {
    $configuration = $this->getConfiguration();
    return CoveoPushApiEndpoint::tryFrom($configuration['push_api_endpoint']) ?? CoveoPushApiEndpoint::Dev;
  }

  /**
   * {@inheritdoc}
   */
  public function viewSettings() {

    $labels = [
      'credential_provider' => $this->t('Credentials storage provider'),
      'storage_key' => $this->t('Credentials storage key'),
      'push_api_endpoint' => $this->t('Push API Endpoint'),
    ];

    $config = $this->getConfiguration();

    $info = [];
    foreach ($config as $name => $label) {
      if (array_key_exists($name, $labels)) {
        $info[] = [
          'label' => $labels[$name],
          'info' => $this->configuration[$name],
        ];
      }
    }
    if (!empty($this->credentials['source_name'])) {
      $info[] = [
        'label' => $this->t('Source Name'),
        'info' => $this->credentials['source_name'],
      ];
    }
    if (!empty($this->credentials['organization_id'])) {
      $info[] = [
        'label' => $this->t('Organization ID'),
        'info' => $this->credentials['organization_id'],
      ];
    }

    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public function updateIndex(IndexInterface $index) {
    /** @var \Drupal\search_api\Item\FieldInterface[] $fields*/
    $fields = $index->getFields(TRUE);

    // Build a set of fields in the current index.
    $index_fields = new CoveoFieldInfoSet([]);
    foreach ($fields as $field_name => $field) {
      // Skip title (a Coveo system field).
      if ($field_name === self::TITLE) {
        continue;
      }
      $index_fields->add(
        new CoveoFieldInfo(
          name: $field_name,
          description: $field->getLabel(),
          type: $this->mapCoveoType($field->getType()),
        )
      );
    }

    $user_fields = $this->coveoClient->loadUserFields();
    // We need to add index fields that are not in current user fields.
    $fields_to_add = $index_fields->difference($user_fields);
    // We need to update fields that are already in current user fields.
    $fields_to_update = $index_fields->intersect($user_fields);

    if (!$fields_to_add->isEmpty()) {
      $fields_created = $this->coveoClient->createFields($fields_to_add);
      // Check fields are created in coveo.
      if ($fields_created) {
        $t_args = ['%field_ids' => implode(", ", $fields_to_add->getFieldIds())];
        $this->messenger()->addStatus($this->t('New fields added in coveo: %field_ids.', $t_args));
        // Log the message.
        $this->logMessage('New fields added in coveo: %field_ids.', $t_args, RfcLogLevel::INFO);
      }
    }

    if (!$fields_to_update->isEmpty()) {
      $fields_updated = $this->coveoClient->updateFields($fields_to_update);
      // Check fields are updated in coveo.
      if ($fields_updated !== FALSE) {
        $t_args = ['%field_ids' => implode(", ", $fields_to_update->getFieldIds())];
        $this->messenger()->addStatus($this->t('Fields updated in coveo: %field_ids.', $t_args));
        // Log the message.
        $this->logMessage('Fields updated in coveo: %field_ids.', $t_args, RfcLogLevel::INFO);
      }
    }
  }

  /**
   * Check if a coveo field needs to be updated.
   *
   * @param mixed $coveo_field
   *   Coveo field.
   * @param mixed $search_api_field
   *   Search_api_coveo Field.
   *
   * @return bool
   *   Return if coveo field is dirty.
   */
  protected function canUpdateField($coveo_field, $search_api_field) {
    $can_update = FALSE;

    // Check the field is not coveo system fields.
    if (isset($coveo_field['system']) && $coveo_field['system'] === FALSE) {
      /* Add multiValueFacet,facet, hierarchicalFacet and sort properties to
       * comparison
       */
      if ($coveo_field['description'] !== $search_api_field['description']) {
        $can_update = TRUE;
      }
    }
    return $can_update;
  }

  /**
   * {@inheritdoc}
   */
  public function removeIndex($index) {
    if (!is_object($index) || empty($index->get('read_only'))) {
      $this->coveoClient->deleteOlderThan();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function indexItems(IndexInterface $index, array $items) {
    $objects = $this->previewItems($index, $items);

    if (count($objects) <= 0) {
      return [];
    }

    /*
     * @see https://docs.coveo.com/en/90/index-content/manage-batches-of-items-in-a-push-source
     */
    $this->coveoClient->startBatch();

    $logger = $this->getLogger();
    $indexed_objects_keys = [];
    foreach ($objects as $object_key => $object) {
      $document_id = $object[self::DOCUMENT_ID];
      if (!empty($document_id)) {
        unset($object[self::DOCUMENT_ID]);
        $document = $this->createCoveoDocument($document_id, $object, $logger);
        $is_added = $this->coveoClient->addDocument($document);
        if ($is_added === TRUE) {
          $indexed_objects_keys[] = $object_key;
        }
        else {
          $variables['%document_id'] = $document_id;
          $variables['%object_key'] = $object_key;
          $this->logMessage('Document %document_id was not sent to Coveo for node %object_key', $variables);
        }
      }
      else {
        $variables['%object_key'] = $object_key;
        $this->logMessage('Coveo Document Id value is not set for %object_key', $variables);
      }
    }
    $batch_uploaded = $this->coveoClient->endBatch();
    if ($batch_uploaded) {
      return $indexed_objects_keys;
    }
    else {
      return [];
    }
  }

  /**
   * Create document to upload.
   *
   * @param string $document_id
   *   Coveo Unique document Id.
   * @param array $object
   *   Object data to index.
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger.
   *
   * @return \Drupal\search_api_coveo\DataStructure\CoveoDocument
   *   The Coveo document to upload.
   */
  protected function createCoveoDocument(string $document_id, array $object, LoggerInterface $logger): CoveoDocument {
    $date = new \DateTime();
    $document = new CoveoDocument($document_id, $date, $date, $logger);

    foreach ($object as $key => $value) {
      if (empty($value)) {
        continue;
      }
      switch ($key) {
        case self::CONTENT_FIELD_ID:
          $document->setContentAndZlibCompress($value);
          break;

        case self::URL_FIELD_ID:
          $document->setClickableUri($value);
          break;

        default:
          $document->addMetadata($key, $value);
          break;
      }
    }
    return $document;
  }

  /**
   * Prepares a single item for indexing.
   *
   * Used as a helper method in indexItem()/indexItems().
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index for which the item is being indexed.
   * @param \Drupal\search_api\Item\ItemInterface $item
   *   The item to index.
   */
  protected function prepareItem(IndexInterface $index, ItemInterface $item) {
    $entity = $this->getEntity($item);

    // Build item to index.
    $item_to_index = [
      'documentType' => 'WebPage',
      'sourceType' => 'Push',
      'documentId' => $this->getDocumentId($index, $item->getId()),
      self::URL_FIELD_ID => $entity->toUrl()->toString(),
    ];

    $item_fields = $item->getFields();
    foreach ($item_fields as $field_id => $field) {
      $values = $field->getValues();
      if ($this->isValueEmpty($values)) {
        continue;
      }

      // Massage the values.
      foreach ($values as $index => $value) {
        // Cast text value to strings.
        if ($value instanceof TextValue) {
          $values[$index] = (string) $value;
        }
      }

      // If the item field is an entity field, determine if it should be
      // included and its cardinality.
      $field_storage_config = $this->getFieldStorageConfig($field);
      if ($field_storage_config) {
        $field_name = $field_storage_config->getName();
        // Make sure the entity has the field.
        if (!($entity instanceof ContentEntityInterface) || !$entity->hasField($field_name)) {
          continue;
        }
        // Reset the value array if the field only stores one value.
        if ($field_storage_config->getCardinality() === 1) {
          $values = reset($values);
        }
      }
      else {
        // Otherwise, reset arrays that contain a single value.
        if (count($values) <= 1) {
          $values = reset($values);
        }
      }

      $item_to_index[$field_id] = $values;
    }
    return $item_to_index;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteItems(?IndexInterface $index, array $item_ids) {
    if (!$index) {
      return;
    }
    foreach ($item_ids as $id) {
      $document_id = $this->getDocumentId($index, $id);
      $this->coveoClient->deleteItem($document_id);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAllIndexItems(?IndexInterface $index = NULL, $datasource_id = NULL) {
    if (!$index) {
      return;
    }
    $this->coveoClient->deleteOlderThan();
  }

  /**
   * {@inheritdoc}
   */
  public function search(QueryInterface $query) {
    $results = $query->getResults();
    $results->setResultItems([]);
    $results->setResultCount(0);
    return $results;
  }

  /**
   * Converts a field type into a Coveo type.
   *
   * @param string $type
   *   The original type.
   *
   * @return string
   *   The new type.
   */
  protected function mapCoveoType($type) {
    switch ($type) {
      case 'boolean':
      case 'string':
      case 'text':
        $mappedType = 'STRING';
        break;

      case 'date':
        $mappedType = 'DATE';
        break;

      case 'integer':
        $mappedType = 'LONG_64';
        break;

      default:
        $mappedType = 'STRING';
    }
    return $mappedType;
  }

  /**
   * Generate a preview of the data being pushed to Coveo.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index for which the item is being indexed.
   * @param \Drupal\search_api\Item\ItemInterface[] $items
   *   The item to index.
   *
   * @return array
   *   An array of item data to be pushed to Coveo.
   */
  public function previewItems(IndexInterface $index, array $items) {
    if (count($items) <= 0) {
      return [];
    }

    foreach ($items as $id => $item) {
      $objects[$id] = $this->prepareItem($index, $item);
    }
    // Let other modules alter objects before sending them to Coveo.
    $this->moduleHandler->alter('search_api_coveo_objects', $objects, $index, $items);
    return $objects;
  }

  /**
   * Get entity field storage for Search API field.
   *
   * @param \Drupal\search_api\Item\FieldInterface $field
   *   A Search API field.
   *
   * @return \Drupal\field\FieldStorageConfigInterface|null
   *   An entity field storage config or NULL.
   */
  protected function getFieldStorageConfig(FieldInterface $field) {
    $field_id = $field->getFieldIdentifier();
    $index = $field->getIndex();
    $field_storage_configs = $this->getFieldStorageConfigs($index);
    return (isset($field_storage_configs[$field_id])) ? $field_storage_configs[$field_id] : NULL;
  }

  /**
   * Get entity field storage for Search API index.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   A Search API index.
   *
   * @return \Drupal\field\FieldStorageConfigInterface[]
   *   entity field storage for Search API index.
   */
  protected function getFieldStorageConfigs(IndexInterface $index) {
    $index_id = $index->id();

    if (!isset($this->fieldStorageConfigs[$index_id])) {
      // Get entity type ids from datasources.
      $datasources = $index->getDatasources();
      $entity_type_ids = [];
      foreach ($datasources as $datasource) {
        $entity_type_ids[$datasource->getEntityTypeId()] = $datasource->getEntityTypeId();
      }

      // Build a lookup table of field names.
      $field_storage_config_ids = [];
      $item_fields = $index->getFields();
      foreach ($item_fields as $field_id => $field) {
        [$entity_field_name] = explode(':', $field->getPropertyPath());
        foreach ($entity_type_ids as $entity_type_id) {
          $field_storage_config_ids[$field_id] = $entity_type_id . '.' . $entity_field_name;
        }
      }

      // Load field storage configs.
      $field_storage_configs = FieldStorageConfig::loadMultiple($field_storage_config_ids);

      $this->fieldStorageConfigs[$index_id] = [];
      foreach ($field_storage_config_ids as $field_id => $field_storage_config_ide) {
        if (isset($field_storage_configs[$field_storage_config_ide])) {
          $this->fieldStorageConfigs[$index_id][$field_id] = $field_storage_configs[$field_storage_config_ide];
        }
      }
    }

    return $this->fieldStorageConfigs[$index_id];
  }

  /**
   * Determine if field store multiple values.
   *
   * @param \Drupal\search_api\Item\FieldInterface $field
   *   A Search API field.
   *
   * @return bool
   *   TRUE if field store multiple values.
   */
  protected function isMultiValue(FieldInterface $field) {
    $field_storage_config = $this->getFieldStorageConfig($field);
    if ($field_storage_config) {
      return ($field_storage_config->getCardinality() !== 1);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Determine if field is a hierarchical facet.
   *
   * @param \Drupal\search_api\Item\FieldInterface $field
   *   A Search API field.
   *
   * @return bool
   *   TRUE if field references a vocabulary with hierarchical terms.
   */
  protected function isHierarchicalFacet(FieldInterface $field) {
    $field_storage_config = $this->getFieldStorageConfig($field);
    if (!$field_storage_config
      || $field_storage_config->getType() !== 'entity_reference'
      || $field_storage_config->getSetting('target_type') !== 'taxonomy_term') {
      return FALSE;
    }

    /** @var \Drupal\taxonomy\TermStorageInterface $taxonomy_storage */
    $taxonomy_storage = $this->entityTypeManager->getStorage('taxonomy_term');

    $properties = ['field_name' => $field_storage_config->getName()];

    /** @var \Drupal\field\FieldConfigInterface[] $field_configs */
    $field_configs = $this->entityTypeManager
      ->getStorage('field_config')
      ->loadByProperties($properties);

    foreach ($field_configs as $field_config) {
      $settings = $field_config->getSettings();
      $parents = ['handler_settings', 'target_bundles'];
      $target_bundles = NestedArray::getValue($settings, $parents) ?: [];
      foreach ($target_bundles as $vid) {
        if ($taxonomy_storage->getVocabularyHierarchyType($vid) !== Vocabulary::HIERARCHY_DISABLED) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * Get Coveo document ID from a Search API item id.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   A Search API index.
   * @param string $item_id
   *   A Search API item id.
   *
   * @return string
   *   A Coveo document ID.
   */
  protected function getDocumentId(IndexInterface $index, $item_id) {
    [$entity_type, $entity_id] = explode('/', $item_id);

    $route_name = 'search_api_coveo.document_id';
    $route_parameters = [
      'index_id' => $index->id(),
      'entity_type' => $entity_type,
      'entity_id' => $entity_id,
    ];
    $route_options = [
      'absolute' => TRUE,
      'language' => $this->languageManager->getDefaultLanguage(),
    ];

    $document_id = Url::fromRoute($route_name, $route_parameters, $route_options)->toString();
    $document_id = str_replace('%3A', ':', $document_id);

    $this->moduleHandler->alter('search_api_coveo_document_id', $document_id, $index, $item_id);

    return $document_id;
  }

  /**
   * Verify if value is truly empty and not just "falsey".
   *
   * @param mixed $value
   *   A value.
   *
   * @return bool
   *   True is value is empty, false if value is not empty.
   */
  protected function isValueEmpty($value) {
    return $value === '' || $value === NULL || $value === [];
  }

  /**
   * Get Coveo Organizations.
   *
   * @return array
   *   Array of organizations.
   *
   * @see https://docs.coveo.com/en/3433/cloud-v2-api-reference/organization-api#operation/getOrganizationGlobalConfigurationUsingGET_2
   */
  protected function getOrganizations() {
    $organizations = $this->coveoClient->getOrganizations();
    return $organizations;
  }

  /**
   * Converts a search api item to the original entity.
   *
   * @param \Drupal\search_api\Item\ItemInterface $item
   *   The item.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   *
   * @throws \Drupal\search_api\SearchApiException
   */
  protected function getEntity(ItemInterface $item): EntityInterface {
    /** @var \Drupal\Core\Entity\Plugin\DataType\EntityAdapter $searchApiObject */
    $searchApiObject = $item->getOriginalObject();
    return $searchApiObject->getEntity();
  }

}
